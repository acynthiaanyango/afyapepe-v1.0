<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicalhistory extends Model
{
    protected $table='self_reported_medical_history';
    
    protected $fillable=['afya_user_id', 'hypertension', 'diabetes', 'heart_attack', 'stroke', 'liver_disease', 'lung_disease', 'bowel_disease', 'eye_disease', 'skin_problems', 'pyschological_problems', 'arthritis_joint_disease', 'gyneocological_disease', 'thyroid_disease'];
}
