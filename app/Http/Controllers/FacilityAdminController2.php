<?php

namespace App\Http\Controllers;
use App\Testprice;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use App\Link;
use App\Ctscan;
use App\Testpricexray;
use App\Testpriceultra;
use App\Testpricemri;


class FacilityAdminController2 extends Controller
{

  //LAB TESTS
  public function addItem(Request $request)
    {
      $rules = array(
                'availability' => 'required|alpha_num',
                'amount' => 'required|alpha_num',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Response::json(array(

                    'errors' => $validator->getMessageBag()->toArray(),
            ));
        } else {
  $fac = DB::table('facility_admin')
      ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
      ->first();


            $data1 = new Testprice();
            $data1->tests_id = $request->tests_id;
            $data1->availability = $request->availability;
            $data1->amount = $request->amount;
            $data1->user_id = $fac->user_id;
            $data1->facility_id = $fac->facilitycode;
            $data1->save();

            $insertedId = $data1->id;

            $data =DB::table('tests')
            ->leftjoin('test_subcategories','tests.sub_categories_id','=','test_subcategories.id')
            ->leftjoin('test_price','tests.id','=','test_price.tests_id')
                  ->select('tests.name','tests.id as testId','test_price.id','test_price.availability',
                  'test_price.amount','test_subcategories.name as sub')
                  ->where('test_price.id',$insertedId)
                  ->first();

            return response()->json($data);
}
    }
    public function readItems(Request $req)
    {
      $data =DB::table('tests')
    //  ->leftjoin('test_price','tests.id','=','test_price.tests_id')
      ->leftjoin('test_subcategories','tests.sub_categories_id','=','test_subcategories.id')
      ->select('tests.name','tests.id as testId','test_subcategories.name as sub')
      ->get();

      return view('facilityadmin.testprice')->with('data',$data);
    }
    public function editItem(Request $req)
    {
      $fac = DB::table('facility_admin')
      ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
      ->first();
      $tpid=$req->id;

        $data1 = Testprice::find($req->id);
        $data1->availability = $req->availability;
        $data1->amount = $req->amount;
        $data1->user_id = $fac->user_id;
        $data1->facility_id = $fac->facilitycode;
        $data1->save();

        $data =DB::table('tests')
        ->leftjoin('test_subcategories','tests.sub_categories_id','=','test_subcategories.id')
       ->leftjoin('test_price','tests.id','=','test_price.tests_id')
              ->select('tests.name','tests.id as testId','test_price.id','test_price.availability',
              'test_price.amount','test_subcategories.name as sub')
              ->where('test_price.id',$tpid)
              ->first();
        return response()->json($data);
    }


//CT TESTS
public function addct(Request $request)
  {
    $rules = array(
              'availability' => 'required|alpha_num',
              'amount' => 'required|alpha_num',
      );
      $validator = Validator::make(Input::all(), $rules);
      if ($validator->fails()) {
          return Response::json(array(

                  'errors' => $validator->getMessageBag()->toArray(),
          ));
      } else {
$fac = DB::table('facility_admin')
    ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
    ->first();


    $ct_scan_id=$request->tests_id;
    $availability=$request->availability;
    $amount=$request->amount;
    $user_id=$fac->user_id;
    $facility_id=$fac->facilitycode;



  $insertedId = DB::table('test_prices_ct_scan')->insertGetId([
        'ct_scan_id'=>$ct_scan_id,
         'availability'=>$availability,
         'amount'=>$amount,
         'user_id'=>$user_id,
         'facility_id'=>$facility_id,
          ]);
$data =DB::table('ct_scan')
        ->leftjoin('test_prices_ct_scan','ct_scan.id','=','test_prices_ct_scan.ct_scan_id')
        ->select('ct_scan.name','ct_scan.id as testId','test_prices_ct_scan.id','test_prices_ct_scan.availability',
        'test_prices_ct_scan.amount')
        ->where('test_prices_ct_scan.id',$insertedId)
        ->first();
return response()->json($data);
}
  }
  public function readct()
  {

$data =DB::table('ct_scan')
->select('ct_scan.name','ct_scan.id as testId')
->get();
      return view('facilityadmin.testpricect')->with('data',$data);
  }
  public function editct(Request $request)
  {

    $fac = DB::table('facility_admin')
    ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
    ->first();

    $tpid=$request->id;
     $data1 = Ctscan::find($request->id);
      $data1->availability = $request->availability;
      $data1->amount = $request->amount;
      $data1->user_id = $fac->user_id;
      $data1->facility_id = $fac->facilitycode;
      $data1->save();

      $data =DB::table('ct_scan')
      ->leftjoin('test_prices_ct_scan','ct_scan.id','=','test_prices_ct_scan.ct_scan_id')
      ->select('ct_scan.name','ct_scan.id as testId','test_prices_ct_scan.id','test_prices_ct_scan.availability',
      'test_prices_ct_scan.amount')
      ->where('test_prices_ct_scan.id',$tpid)
      ->first();
      return response()->json($data);
  }

  //xray TESTS
  public function addxray(Request $request)
  {
    $rules = array(
              'availability' => 'required',
              'amount' => 'required|alpha_num',
      );
      $validator = Validator::make(Input::all(), $rules);
      if ($validator->fails()) {
          return Response::json(array(

                  'errors' => $validator->getMessageBag()->toArray(),
          ));
      } else {

$fac = DB::table('facility_admin')
    ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
    ->first();


    $xray_id=$request->tests_id;
    $availability=$request->availability;
    $amount=$request->amount;
    $user_id=$fac->user_id;
    $facility_id=$fac->facilitycode;

  $insertedId = DB::table('test_prices_xray')->insertGetId([
         'xray_id'=>$xray_id,
         'availability'=>$availability,
         'amount'=>$amount,
         'facility_id'=>$facility_id,
         'user_id'=>$user_id,
          ]);

$data =DB::table('xray')
        ->join('test_prices_xray','xray.id','=','test_prices_xray.xray_id')
        ->select('xray.name','xray.id as testId','test_prices_xray.id','test_prices_xray.availability','test_prices_xray.amount')
        ->where('test_prices_xray.id',$insertedId)
        ->first();

    return response()->json($data);
}
    }
    public function readxray(Request $req)
    {
  $data =DB::table('xray')->select('name','id as testId')->get();

        return view('facilityadmin.testpricexray')->with('data',$data);
    }

    public function readotherIm(Request $req)
    {
  $data =DB::table('other_tests')->select('name','id as testId')->get();

        return view('facilityadmin.otherIm')->with('data',$data);
    }

    public function editxray(Request $req)
    {
      $fac = DB::table('facility_admin')
      ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
      ->first();
      $tpid=$req->id;

        $data1 = Testpricexray::find($req->id);
        $data1->availability = $req->availability;
        $data1->amount = $req->amount;
        $data1->user_id = $fac->user_id;
        $data1->facility_id = $fac->facilitycode;
        $data1->save();

          $data =DB::table('xray')
              ->leftjoin('test_prices_xray','xray.id','=','test_prices_xray.xray_id')
              ->select('xray.name','xray.id as testId','test_prices_xray.id','test_prices_xray.availability',
                    'test_prices_xray.amount')
                    ->where('test_prices_xray.id',$tpid)
                    ->first();
        return response()->json($data);
    }
    public function editother(Request $req)
    {
      $fac = DB::table('facility_admin')
      ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
      ->first();
      $tpid=$req->id;
      $availability = $req->availability;
      $amount = $req->amount;
      $user_id = $fac->user_id;
      $facility_id = $fac->facilitycode;
      $updates = DB::table('test_prices_other')->where('id',$tpid)
      ->update([
              'availability'=>$availability,
               'amount'=>$amount,
               'facility_id'=>$facility_id,
               'user_id'=>$user_id,
               'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
                ]);

          $data =DB::table('other_tests')
              ->leftjoin('test_prices_other','other_tests.id','=','test_prices_other.other_id')
              ->select('other_tests.name','other_tests.id as testId','test_prices_other.id','test_prices_other.availability',
                    'test_prices_other.amount')
                    ->where('test_prices_other.id',$tpid)
                    ->first();
        return response()->json($data);
    }
    //xray TESTS
    public function saveother(Request $request)
    {
      $rules = array(
                'availability' => 'required',
                'amount' => 'required|alpha_num',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Response::json(array(

                    'errors' => $validator->getMessageBag()->toArray(),
            ));
        } else {

  $fac = DB::table('facility_admin')
      ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
      ->first();


      $xray_id=$request->tests_id;
      $availability=$request->availability;
      $amount=$request->amount;
      $user_id=$fac->user_id;
      $facility_id=$fac->facilitycode;

    $insertedId = DB::table('test_prices_other')->insertGetId([
           'other_id'=>$xray_id,
           'availability'=>$availability,
           'amount'=>$amount,
           'facility_id'=>$facility_id,
           'user_id'=>$user_id,
            ]);


          $data =DB::table('other_tests')
              ->leftjoin('test_prices_other','other_tests.id','=','test_prices_other.other_id')
              ->select('other_tests.name','other_tests.id as testId','test_prices_other.id','test_prices_other.availability',
                    'test_prices_other.amount')
                    ->where('test_prices_other.id',$insertedId)
                    ->first();
      return response()->json($data);
  }
      }
    //MRI TESTS
    public function addmri(Request $request)
      {
        $rules = array(
                  'availability' => 'required',
                  'amount' => 'required|alpha_num',
          );
          $validator = Validator::make(Input::all(), $rules);
          if ($validator->fails()) {
              return Response::json(array(

                      'errors' => $validator->getMessageBag()->toArray(),
              ));
          } else {
    $fac = DB::table('facility_admin')
        ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
        ->first();


        $mri_id=$request->tests_id;
        $availability=$request->availability;
        $amount=$request->amount;
        $user_id=$fac->user_id;
        $facility_id=$fac->facilitycode;



      $insertedId = DB::table('test_prices_mri')->insertGetId([
            'mri_id'=>$mri_id,
             'availability'=>$availability,
             'amount'=>$amount,
             'user_id'=>$user_id,
             'facility_id'=>$facility_id,
              ]);
    $data =DB::table('mri_tests')
          ->join('test_prices_mri','mri_tests.id','=','test_prices_mri.mri_id')
          ->select('mri_tests.name','mri_tests.id as testId','test_prices_mri.id','test_prices_mri.availability',
          'test_prices_mri.amount')
            ->where([['test_prices_mri.id',$insertedId],['test_prices_mri.facility_id',$facility_id]])
            ->first();
    return response()->json($data);
    }
      }
      public function readmri()
      {

            $data =DB::table('mri_tests')
                  ->select('mri_tests.name','mri_tests.id as testId')
                  ->get();

          return view('facilityadmin.testpricemri')->with('data',$data);
      }

      public function upimages()
      {

            $data =DB::table('mri_tests')
                  ->select('mri_tests.name','mri_tests.id as testId')
                  ->get();
return view('facilityadmin.upimages')->with('data',$data);
      }

      public function upimagespst(Request $request) {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            ]);

      $destinatonPath = '';
      $filename = '';
      $facid= DB::table('facility_admin')->where('user_id', '=', Auth::user()->id)->first();
    $facility=$facid->facilitycode;

      $file = Input::file('image');
      $destinationPath = public_path().'/img/logos/';
      $filename = str_random(6).'_'.$file->getClientOriginalName();
      $uploadSuccess = $file->move($destinationPath, $filename);


      if(Input::hasFile('image')){

$insertedId = DB::table('logo_imgs')->insertGetId([
'facility' => $facility,
'directory' => $filename,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}



return view('facilityadmin.upimages');
        }






      public function editmri(Request $req)
      {
        $fac = DB::table('facility_admin')
        ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
        ->first();

        $tpid=$req->id;
          $data1 = Testpricemri::find($req->id);
          $data1->availability = $req->availability;
          $data1->amount = $req->amount;
          $data1->user_id = $fac->user_id;
          $data1->facility_id = $fac->facilitycode;
          $data1->save();

          $data = DB::table('mri_tests')
                ->join('test_prices_mri','mri_tests.id','=','test_prices_mri.mri_id')
                ->select('mri_tests.name','mri_tests.id as testId','test_prices_mri.id','test_prices_mri.availability',
                'test_prices_mri.amount')
                  ->where([['test_prices_mri.id',$tpid],['test_prices_mri.facility_id',$fac->facilitycode]])
                  ->first();
          return response()->json($data);
      }

      //ULTRA TESTS
      public function addultra(Request $request)
        {
          $rules = array(
                    'availability' => 'required|alpha_num',
                    'amount' => 'required|alpha_num',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Response::json(array(

                        'errors' => $validator->getMessageBag()->toArray(),
                ));
            } else {
      $fac = DB::table('facility_admin')
          ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
          ->first();


          $ultrasound_id=$request->tests_id;
          $availability=$request->availability;
          $amount=$request->amount;
          $user_id=$fac->user_id;
          $facility_id=$fac->facilitycode;



        $insertedId = DB::table('test_prices_ultrasound')->insertGetId([
              'ultrasound_id'=>$ultrasound_id,
               'availability'=>$availability,
               'amount'=>$amount,
               'user_id'=>$user_id,
               'facility_id'=>$facility_id,

                ]);
      $data =DB::table('ultrasound')
              ->leftjoin('test_prices_ultrasound','ultrasound.id','=','test_prices_ultrasound.ultrasound_id')
              ->select('ultrasound.name','ultrasound.id as testId','test_prices_ultrasound.id','test_prices_ultrasound.availability',
              'test_prices_ultrasound.amount')
              ->where('test_prices_ultrasound.id',$insertedId)
              ->first();
      return response()->json($data);
      }
        }
        public function readultra(Request $req)
        {

          $data =DB::table('ultrasound')->select('ultrasound.name','ultrasound.id as testId')->get();

                                  // $data =DB::table('ultrasound')
                                  //               ->leftjoin('test_prices_ultrasound','ultrasound.id','=','test_prices_ultrasound.ultrasound_id')
                                  //               ->select('ultrasound.name','ultrasound.id as testId','test_prices_ultrasound.id','test_prices_ultrasound.availability',
                                  //               'test_prices_ultrasound.amount')
                                  //               ->get();
            return view('facilityadmin.testpriceultra')->with('data',$data);
        }
        public function editultra(Request $req)
        {
          $fac = DB::table('facility_admin')
          ->select('user_id','facilitycode')->where('user_id', Auth::user()->id)
          ->first();

          $tpid=$req->id;
            $data1 = Testpriceultra::find($req->id);
            $data1->availability = $req->availability;
            $data1->amount = $req->amount;
            $data1->user_id = $fac->user_id;
            $data1->facility_id = $fac->facilitycode;
            $data1->save();

            $data = DB::table('ultrasound')
              ->leftjoin('test_prices_ultrasound','ultrasound.id','=','test_prices_ultrasound.ultrasound_id')
              ->select('ultrasound.name','ultrasound.id as testId','test_prices_ultrasound.id','test_prices_ultrasound.availability',
              'test_prices_ultrasound.amount')
              ->where('test_prices_ultrasound.id',$tpid)
              ->first();
            return response()->json($data);
        }



public function Discounts($id)
{

  $data =DB::table('tests')
  ->leftjoin('test_price','tests.id','=','test_price.tests_id')
  ->leftjoin('test_subcategories','tests.sub_categories_id','=','test_subcategories.id')
  ->select('tests.name','tests.id as testId','test_price.id','test_price.availability','test_price.amount',
  'test_subcategories.name as sub','test_price.facility_id')
  ->Where('tests.id',$id)
  ->first();
    return view('facilityadmin.discount')->with('data',$data);
}
public function adddiscounts(Request $request)
  {
    $reason=$request->reason;
    $amount=$request->amount;
    $test_price_id=$request->test_price_id;
    $facility_id=$request->facility_id;
    $user_id=$request->user_id;
    $status=$request->status;
    $testId=$request->testId;

$discount = DB::table('lab_test_discount')->insert([
        'reason'=>$reason,
         'amount'=>$amount,
         'test_price_id'=>$test_price_id,
         'facility_id'=>$facility_id,
         'user_id'=>$user_id,
          'status'=>$status,
          'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
         'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
          ]);
  return redirect()->action('FacilityAdminController2@Discounts', ['id' => $testId]);
}

public function discountupdate(Request $request)
  {
    $user_id=Auth::user()->id;
$amount=$request->amount;
    $test_discount_id=$request->test_discount_id;
    $status=$request->status;
    $testId=$request->testId;

$discount = DB::table('lab_test_discount')->where('id',$test_discount_id)
->update([
         'amount'=>$amount,
         'user_id'=>$user_id,
        'status'=>$status,
         'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
          ]);
  return redirect()->action('FacilityAdminController2@Discounts', ['id' => $testId]);
}






}
