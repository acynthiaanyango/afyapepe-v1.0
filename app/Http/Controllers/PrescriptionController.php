<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Input;
use App\Prescription;
use App\Prescription_detail;
use Auth;
use Carbon\Carbon;
use App\Druglist;
class PrescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function diagnoses(Request $request)
    {
    $Now = Carbon::now();
    $appointment=$request->get('appointment_id');
    $state = $request->get('state');
    $care=$request->get('care');
    $ptdid =$request->get('ptdid');
    $disease_id = $request->get('disease');


    $prevapp = DB::table('appointments')
    ->select('last_app_id')
    ->where('id',$appointment)
    ->first();
  $appointment2 = $prevapp->last_app_id;
     // Inserting  supportive care
     if ($care) {
    $supportiveCare= DB::table('patient_supp_care')->insert([
                       'name' => $care,
                       'appointment_id' => $appointment,
                          ]);
              }
// Inserting  diagnosis
$pttids= DB::table('patient_diagnosis')
->select('disease_id')
->where([['appointment_id',$appointment2],
              ['disease_id', '=',$disease_id],])
->orwhere([['appointment_id',$appointment],
    ['disease_id', '=',$disease_id],])
->first();


    if (is_null($pttids)) {
     $diagnosis= DB::table('patient_diagnosis')->insert([
                        'afya_user_id' => $request->get('afya_user_id'),
                        'disease_id' => $request->get('disease'),
                        'level' => $request->get('level'),
                        'state' => $request->get('state'),
                        'severity' => $request->get('severity'),
                        'chronic' => $request->get('chronic'),
                        'appointment_id' => $request->get('appointment_id'),
                        'date_diagnosed' => $Now,

]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 12,]);

if ($ptdid) {
DB::table('patient_test_details')
          ->where('id',$ptdid)
          ->update(['confirm'=>'Y']);
}
if ($state == 'Discharge') {
return redirect()->route('disdiagnosis', ['id' => $appointment]);
} elseif ($state =='Normal'){

  $tNY = DB::table('patient_test_details')
  ->where([['appointment_id',$appointment2],['confirm','N'],])
  ->first();
if ($tNY){
return redirect()->route('testes', ['id' => $appointment]);
}else{
return redirect()->route('medicines', ['id' => $appointment]);
}

}

}

public function quickdiag(Request $request)
{
$Now = Carbon::now();
$appointment=$request->appointment_id;
$state = $request->state;
$care=$request->care;
$disease_id = $request->disease;

  $disease_id = $request->disease;
  $afya_user_id = $request->afya_user_id;
  $disease_id = $request->disease;
  $level = $request->level;
  $state = $request->state;
  $severity = $request->severity;
  $chronic = $request->chronic;
  $appointment_id = $request->appointment_id;

 // Inserting  supportive care
          if ($care) {
          $supportiveCare= DB::table('patient_supp_care')->insert([
          'name' => $care,
          'appointment_id' => $appointment,
          ]);
                 }
// Inserting  diagnosis
$pttids= DB::table('patient_diagnosis')
->select('disease_id')
->where([
          ['appointment_id',$appointment],
          ['disease_id', '=',$disease_id],
])
->first();
if (is_null($pttids)) {

 $diagnosis= DB::table('patient_diagnosis')->insert([
                    'afya_user_id' => $afya_user_id,
                    'disease_id' => $disease_id,
                    'level' => $level,
                    'state' => $state,
                    'severity' => $severity,
                    'chronic' => $chronic,
                    'appointment_id' => $appointment_id,
                    'date_diagnosed' => $Now,
         ]);
     }
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 12,]);

return redirect()->action('PatientTestController@diagnoses', ['id' => $appointment]);

}


public function confradiology(Request $request)
{
  $Now = Carbon::now();
$appointment=$request->appointment_id;
$state = $request->state;
$notes =$request->note;
$radiology =$request->radiology;
$rtdid =$request->rtdid;

if ($radiology) {
 $diagnosis= DB::table('patient_diagnosis')->insert([
                    'state' => $state,
                    'radiology' => $radiology,
                    'notes'=> $notes,
                    'appointment_id' => $appointment,
                    'date_diagnosed' => $Now,

]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 12,]);

if ($rtdid) {
DB::table('radiology_test_details')
      ->where('id',$rtdid)
      ->update(['confirm'=>'Y']);
}
return redirect()->route('alltestes', ['id' => $appointment]);
}

    public function prescriptions($id)
    {
      $data['patientD'] =DB::table('appointments')
      ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
      ->leftjoin('dependant','appointments.persontreated','=','dependant.id')
      ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
      ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
      ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
        'dependant.firstName as dep1name','dependant.secondName as dep2name','dependant.gender as depgender',
        'dependant.dob as depdob','facilities.FacilityName','facilities.set_up','patient_admitted.condition')
      ->where('appointments.id',$id)
      ->get();

      $data['Pdiagnosis']=DB::table('patient_diagnosis')
      // ->leftjoin('icd10_option','patient_diagnosis.disease_id','=','icd10_option.id')
      ->leftjoin('severity','patient_diagnosis.severity','=','severity.id')
      ->select('patient_diagnosis.disease_id as name','severity.name as severity','patient_diagnosis.id'
      ,'patient_diagnosis.level','patient_diagnosis.radiology','patient_diagnosis.notes','patient_diagnosis.chronic')
      ->where([
                    ['patient_diagnosis.appointment_id',$id],
                    ['patient_diagnosis.state', '=', 'Normal'],
                   ])
      ->get();

      $data['presczs'] =DB::table('prescriptions')
     ->join('prescription_details','prescriptions.id','=','prescription_details.presc_id')
     ->leftjoin('route','prescription_details.routes','=','route.id')
     ->leftjoin('icd10_option','prescription_details.diagnosis','=','icd10_option.id')
     ->leftjoin('frequency','prescription_details.frequency','=','frequency.id')
      ->select('prescription_details.id as presc_details_id','prescription_details.drug_id','prescription_details.strength_unit','route.abbreviation',
      'route.name as rotename','frequency.name as frequencyname','icd10_option.name','icd10_option.id','prescriptions.id as prescid')
      ->where([['prescriptions.appointment_id',$id],['prescription_details.deleted',0]])
      ->get();



      $cript =DB::table('appointments')
      ->leftjoin('prescriptions','prescriptions.appointment_id','=','appointments.id')
     ->select('prescriptions.id','appointments.afya_user_id')
     ->where('appointments.id',$id)->first();

      $data['allpresczs'] =DB::table('appointments')
      ->join('prescriptions','appointments.id','=','prescriptions.appointment_id')
     ->join('prescription_details','prescriptions.id','=','prescription_details.presc_id')
     ->leftjoin('route','prescription_details.routes','=','route.id')
     ->leftjoin('icd10_option','prescription_details.diagnosis','=','icd10_option.id')
     ->leftjoin('frequency','prescription_details.frequency','=','frequency.id')
      ->select('prescription_details.id as presc_details_id','prescription_details.drug_id','prescription_details.strength_unit','route.abbreviation',
      'route.name as rotename','frequency.name as frequencyname','icd10_option.name','icd10_option.id','prescriptions.id as prescid')
      ->where([['appointments.afya_user_id',$cript->afya_user_id],['prescription_details.deleted',0]])
      ->get();
    ;
      return view('doctor.prescription',$data)->with('cript',$cript);

    }

    public function procedures($id)
    {
      $patientD =DB::table('appointments')
      ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
      ->leftjoin('dependant','appointments.persontreated','=','dependant.id')
      ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
      ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
      ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
        'dependant.firstName as dep1name','dependant.secondName as dep2name','dependant.gender as depgender',
        'dependant.dob as depdob','facilities.FacilityName','facilities.set_up','patient_admitted.condition')
      ->where('appointments.id',$id)
      ->get();

      $Pdiagnosis=DB::table('patient_diagnosis')
      ->leftjoin('icd10_option','patient_diagnosis.disease_id','=','icd10_option.id')
      ->leftjoin('severity','patient_diagnosis.severity','=','severity.id')
      ->select('icd10_option.name','patient_diagnosis.level',
      'severity.name as severity','icd10_option.id')

      ->where([
                    ['patient_diagnosis.appointment_id',$id],
                    ['patient_diagnosis.state', '=', 'Normal'],

                   ])
      ->get();

      return view('doctor.procedure')->with('patientD',$patientD)->with('Pdiagnosis',$Pdiagnosis);

    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

   protected function store(Request $request)
   {
    $appid=$request->appointment;
    $facility_id=$request->facility_id;

  $pttids= Prescription::where('appointment_id',$appid)
       ->first();

      if (is_null($pttids)) {
      //  - add new
      $Prescription=Prescription::create([
           'appointment_id' => $appid,
           'facility_id' =>$facility_id,
           'filled_status' => 0,  ]);
      $id=$Prescription->id;
      DB::table('appointments')->where('id', $appid)
      ->update(['p_status' => 13,]);


      } else {
      // Already exist - get the id the existing
       $id =$pttids->id;
      }

$drug=$request->drug;
$routes=$request->routes;
$frequent=$request->frequent;
$strength=$request->strength;
$strength_unit=$request->unit;
$state =$request->state;
$disease =$request->disease;

if ($drug) {

    $insertedId = DB::table('prescription_details')->insertGetId([
           'presc_id' => $id,
           'diagnosis' => $disease,
           'state' => $state,
           'diagnosis' => $disease,
           'drug_id' => $drug,

           'strength' => $strength,
           'strength_unit' => $strength_unit,
           'routes' => $routes,
           'frequency' => $frequent,
           'is_filled' => 0,
       ]);
  }

    return redirect()->action('PrescriptionController@prescriptions', ['id' => $appid]);

   }


   public function destroypresc($id)
   {
     $pttd=DB::table('prescription_details')
     ->Join('prescriptions', 'prescription_details.presc_id', '=', 'prescriptions.id')
     ->select('prescriptions.appointment_id')
     ->where('prescription_details.id',$id)
     ->first();
$appid =$pttd->appointment_id;
     //DB::table("prescription_details")->where('id',$id)->delete();
     DB::table("prescription_details")->where('id',$id)->update(array('deleted'=>1));

    return redirect()->action('PrescriptionController@prescriptions', ['id' => $appid]);

    }

          //ADD Patient Procedures
          public function addproc(Request $request)
            {

              $proc_id=$request->proc_id;
              $appId=$request->appId;
              $notes=$request->notes;
              $status=$request->status;

            $proc = DB::table('patient_procedure')->insert([
                  'appointment_id'=>$appId,
                   'procedure_id'=>$proc_id,
                   'notes'=>$notes,
                   'status'=>$status,
                   ]);

           $data =DB::table('procedures')
                    ->select('id','code','icd10_codes','description')
                    ->where('procedures.id',$proc_id)
                    ->first();

          return response()->json($data);
            }

            //ADD Patient Procedures
            public function editproc(Request $request)
              {

                $proc_id=$request->id;
                $docId=$request->doc_id;
                $status=$request->stat;
                $dat2=$request->datp;

                DB::table('patient_procedure')
                          ->where('id', $proc_id)
                          ->update([
                            'doc_id' => $docId,
                            'status'=> $status,
                            'procedure_date'=>$dat2
                          ]);

             $data =DB::table('patient_procedure')
                      ->leftjoin('procedures','patient_procedure.procedure_id','=','procedures.id')
                      ->select('patient_procedure.id','patient_procedure.created_at','patient_procedure.notes','patient_procedure.procedure_date',
                      'patient_procedure.status','procedures.code','procedures.icd10_codes','procedures.description')
                        ->where('patient_procedure.id',$proc_id)
                      ->first();

            return response()->json($data);
              }
              public function deleteproc(Request $request)
                {
                  $proc_id=$request->id;
                  $data =DB::table('patient_procedure')
                           ->leftjoin('procedures','patient_procedure.procedure_id','=','procedures.id')
                           ->select('patient_procedure.id','patient_procedure.created_at','patient_procedure.notes','patient_procedure.procedure_date',
                           'patient_procedure.status','procedures.code','procedures.icd10_codes','procedures.description')
                             ->where('patient_procedure.id',$proc_id)
                           ->first();
   $delete=DB::table('patient_procedure')->where('id', $proc_id)->delete();

                  return response()->json($data);
                  }

                  public function allPosts(Request $request)
                      {

                          $columns = array(
                                              0 =>'id',
                                              1 =>'drugname',
                                              2=> 'Ingridients',
                                              3=> 'strength_perdose',
                                              4=> 'Manufacturer',
                                          );

                          $totalData = Prescription::count();

                          $totalFiltered = $totalData;

                          $limit = $request->input('length');
                          $start = $request->input('start');
                          $order = $columns[$request->input('order.0.column')];
                          $dir = $request->input('order.0.dir');

                          if(empty($request->input('search.value')))
                          {
                              $posts = Prescription::offset($start)
                                           ->limit($limit)
                                           ->orderBy($order,$dir)
                                           ->get();
                          }
                          else {
                              $search = $request->input('search.value');

                              $posts =  Post::where('id','LIKE',"%{$search}%")
                                              ->orWhere('drugname', 'LIKE',"%{$search}%")
                                              ->offset($start)
                                              ->limit($limit)
                                              ->orderBy($order,$dir)
                                              ->get();

                              $totalFiltered = Post::where('id','LIKE',"%{$search}%")
                                               ->orWhere('drugname', 'LIKE',"%{$search}%")
                                               ->count();
                          }

                          $data = array();
                          if(!empty($posts))
                          {
                              foreach ($posts as $post)
                              {
                                  $show =  route('posts.show',$post->id);
                                  $edit =  route('posts.edit',$post->id);

                                  $nestedData['id'] = $post->id;
                                  $nestedData['drugname'] = $post->drugname;
                                  $nestedData['Ingridients'] = substr(strip_tags($post->Ingridients),0,50)."...";
                                  $nestedData['strength_perdose'] = $post->strength_perdose;
                                  $nestedData['Manufacturer'] = $post->Manufacturer;
                                  $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                                            &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
                                  $data[] = $nestedData;

                              }
                          }

                          $json_data = array(
                                      "draw"            => intval($request->input('draw')),
                                      "recordsTotal"    => intval($totalData),
                                      "recordsFiltered" => intval($totalFiltered),
                                      "data"            => $data
                                      );

                          echo json_encode($json_data);

                      }


      public function prescdetails($id)
        {
$prescriptions =DB::table('prescriptions')
             ->join('prescription_details','prescriptions.id','=','prescription_details.presc_id')
             ->join('druglists','prescription_details.drug_id','=','druglists.id')
             ->leftjoin('prescription_filled_status','prescription_details.id','=','prescription_filled_status.presc_details_id')
             ->leftjoin('substitute_presc_details','prescription_filled_status.substitute_presc_id','=','substitute_presc_details.id')
             ->leftjoin('pharmacy','prescription_filled_status.outlet_id','=','pharmacy.id')
           ->select('druglists.drugname','prescriptions.created_at','prescription_filled_status.substitute_presc_id',
            'prescription_filled_status.start_date','prescription_filled_status.end_date','prescription_filled_status.dose_given','prescription_filled_status.quantity',
            'prescription_details.strength','prescription_details.routes','prescription_details.frequency','prescription_details.strength_unit',
            'substitute_presc_details.strength as substrength','substitute_presc_details.routes as subroutes',
            'substitute_presc_details.frequency as subfrequency','substitute_presc_details.strength_unit as substrength_unit','substitute_presc_details.drug_id as subdrug_id',
            'pharmacy.name as pharmacy')
            ->where('prescriptions.id',$id)
             ->get();

          $usedetails=DB::table('prescriptions')
                ->join('appointments','prescriptions.appointment_id','=','appointments.id')
                ->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
                ->Join('doctors', 'appointments.doc_id', '=', 'doctors.id')
                ->select('appointments.id as appid','appointments.persontreated','appointments.afya_user_id','facilities.FacilityName',
                'doctors.name as docname','prescriptions.created_at')
                ->where('prescriptions.id',$id)
                ->first();
    return view('doctor.done.prscdetails')->with('prescriptions',$prescriptions)->with('usedetails',$usedetails);
    }


          public function prescdetails2($id)
            {
    $prescriptions =DB::table('prescriptions')
                 ->join('prescription_details','prescriptions.id','=','prescription_details.presc_id')
                 ->join('druglists','prescription_details.drug_id','=','druglists.id')
                 ->leftjoin('prescription_filled_status','prescription_details.id','=','prescription_filled_status.presc_details_id')
                 ->leftjoin('substitute_presc_details','prescription_filled_status.substitute_presc_id','=','substitute_presc_details.id')
                 ->leftjoin('pharmacy','prescription_filled_status.outlet_id','=','pharmacy.id')
      ->select('druglists.drugname','prescriptions.created_at','prescription_filled_status.substitute_presc_id',
                'prescription_filled_status.start_date','prescription_filled_status.end_date','prescription_filled_status.dose_given','prescription_filled_status.quantity',
                'prescription_details.strength','prescription_details.routes','prescription_details.frequency','prescription_details.strength_unit',
                'substitute_presc_details.strength as substrength','substitute_presc_details.routes as subroutes',
                'substitute_presc_details.frequency as subfrequency','substitute_presc_details.strength_unit as substrength_unit','substitute_presc_details.drug_id as subdrug_id',
                'pharmacy.name as pharmacy')
                ->where('prescriptions.id',$id)
                 ->get();

              $usedetails=DB::table('prescriptions')
                    ->join('appointments','prescriptions.appointment_id','=','appointments.id')
                    ->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
                    ->Join('doctors', 'appointments.doc_id', '=', 'doctors.id')
                    ->select('appointments.id as appid','appointments.persontreated','appointments.afya_user_id','facilities.FacilityName',
                    'doctors.name as docname','prescriptions.created_at')
                    ->where('prescriptions.id',$id)
                    ->first();
        return view('doctor.done.prscdetails2')->with('prescriptions',$prescriptions)->with('usedetails',$usedetails);
        }
        public function printpresc($id)
          {
            $u_id = Auth::user()->id;
            $facility = DB::table('facility_doctor')->select('facilitycode')->where('user_id', $u_id)->first();
            $facility =$facility->facilitycode;

            $data['receipt'] = DB::table('appointments')
                     ->leftJoin('afya_users', 'appointments.afya_user_id', '=', 'afya_users.id')
                     ->leftJoin('dependant', 'appointments.persontreated', '=', 'dependant.id')
                     ->leftJoin('doctors', 'doctors.id', '=', 'appointments.doc_id')
                     ->leftjoin('facility_doctor', 'facility_doctor.doctor_id', '=', 'doctors.id')
                     ->leftjoin('facilities', 'facilities.FacilityCode', '=', 'facility_doctor.facilitycode')
                     ->select('afya_users.firstname', 'afya_users.secondName', 'dependant.firstName as dep_fname','dependant.secondName as dep_lname',
                     'doctors.name AS doc_name', 'facilities.FacilityName',
                     'appointments.id as appid', 'appointments.persontreated', 'appointments.appointment_date')
                     ->where([
                       ['appointments.id', '=', $id],
                     ])
                     ->first();


                     $data['presczs'] =DB::table('prescriptions')
                    ->join('prescription_details','prescriptions.id','=','prescription_details.presc_id')
                    ->leftjoin('route','prescription_details.routes','=','route.id')
                    ->leftjoin('icd10_option','prescription_details.diagnosis','=','icd10_option.id')
                    ->leftjoin('frequency','prescription_details.frequency','=','frequency.id')
                     ->select('prescription_details.id as presc_details_id','prescription_details.drug_id','prescription_details.strength_unit','route.abbreviation',
                     'route.name as rotename','frequency.name as frequencyname','icd10_option.name','icd10_option.id','prescriptions.id as prescid')
                     ->where([['prescriptions.appointment_id',$id],['prescription_details.deleted',0]])
                     ->get();

                     $data['prb'] =DB::table('prescriptions')->select('id','created_at')->where('prescriptions.appointment_id',$id)
                     ->first();

            return view('doctor.prscprint',$data);
          }
}
