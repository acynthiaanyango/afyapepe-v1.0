<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Patienttest;
use Illuminate\Support\Facades\Input;
use Auth;
use Carbon\Carbon;

class PatientTestController2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function xrayreports(Request $request)
     {
      $appointment=$request->get('appointment_id');
      $rtd_id = $request->get('rtd_id');

       $tsts1 = DB::table('radiology_test_details')
       ->Join('appointments', 'radiology_test_details.appointment_id', '=', 'appointments.id')
      ->Join('patient_test', 'radiology_test_details.patient_test_id', '=', 'patient_test.id')
    ->Join('facility_test', 'radiology_test_details.user_id', '=', 'facility_test.user_id')
       ->Join('facilities', 'facility_test.facilitycode', '=', 'facilities.FacilityCode')
      ->Join('test_categories', 'radiology_test_details.test_cat_id', '=', 'test_categories.id')
      ->Join('xray', 'radiology_test_details.test', '=', 'xray.id')
   ->select('appointments.*','appointments.persontreated','test_categories.name as category',
      'radiology_test_details.*','radiology_test_details.id as rtdid','facility_test.secondname',
      'xray.name as tstname','xray.technique','xray.id as xrayid','facilities.FacilityName','facility_test.firstname')
       ->where('radiology_test_details.id', '=',$rtd_id)
       ->first();

       $patientD=DB::table('appointments')
       ->leftjoin('triage_details','appointments.id','=','triage_details.appointment_id')
      ->leftjoin('triage_infants','appointments.id','=','triage_infants.appointment_id')
    ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
     ->leftjoin('dependant','appointments.persontreated','=','dependant.id')
     ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
       ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
       ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
         'dependant.firstName as dep1name','dependant.secondName as dep2name','dependant.gender as depgender',
         'dependant.dob as depdob','facilities.FacilityName','facilities.set_up','patient_admitted.condition',
         'triage_details.lmp as almp','triage_details.pregnant as apregnant','triage_infants.lmp as dlmp','triage_infants.pregnant as dpregnant')
       ->where('appointments.id',$appointment)
       ->get();
    return view('doctor.tests.reportxray')->with('tsts1',$tsts1)->with('patientD',$patientD);
     }

     public function ctreports(Request $request)
     {
      $appointment=$request->get('appointment_id');
      $rtd_id = $request->get('rtd_id');

       $tsts1 = DB::table('radiology_test_details')
       ->Join('appointments', 'radiology_test_details.appointment_id', '=', 'appointments.id')
      ->Join('patient_test', 'radiology_test_details.patient_test_id', '=', 'patient_test.id')
    ->Join('facility_test', 'radiology_test_details.user_id', '=', 'facility_test.user_id')
       ->Join('facilities', 'facility_test.facilitycode', '=', 'facilities.FacilityCode')
      ->Join('test_categories', 'radiology_test_details.test_cat_id', '=', 'test_categories.id')
      ->Join('ct_scan', 'radiology_test_details.test', '=', 'ct_scan.id')
   ->select('appointments.*','appointments.persontreated','test_categories.name as category',
      'radiology_test_details.*','radiology_test_details.id as rtdid','facility_test.secondname',
      'ct_scan.name as tstname','ct_scan.technique','ct_scan.id as ctid','facilities.FacilityName','facility_test.firstname')
       ->where('radiology_test_details.id', '=',$rtd_id)
       ->first();

       $patientD=DB::table('appointments')
       ->leftjoin('triage_details','appointments.id','=','triage_details.appointment_id')
      ->leftjoin('triage_infants','appointments.id','=','triage_infants.appointment_id')
    ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
     ->leftjoin('dependant','appointments.persontreated','=','dependant.id')
     ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
       ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
       ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
         'dependant.firstName as dep1name','dependant.secondName as dep2name','dependant.gender as depgender',
         'dependant.dob as depdob','facilities.FacilityName','facilities.set_up','patient_admitted.condition',
         'triage_details.lmp as almp','triage_details.pregnant as apregnant','triage_infants.lmp as dlmp','triage_infants.pregnant as dpregnant')
       ->where('appointments.id',$appointment)
       ->get();
    return view('doctor.tests.reportct')->with('tsts1',$tsts1)->with('patientD',$patientD);
     }

     public function mrireports(Request $request)
     {
      $appointment=$request->get('appointment_id');
      $rtd_id = $request->get('rtd_id');

       $tsts1 = DB::table('radiology_test_details')
       ->Join('appointments', 'radiology_test_details.appointment_id', '=', 'appointments.id')
      ->Join('patient_test', 'radiology_test_details.patient_test_id', '=', 'patient_test.id')
    ->Join('facility_test', 'radiology_test_details.user_id', '=', 'facility_test.user_id')
       ->Join('facilities', 'facility_test.facilitycode', '=', 'facilities.FacilityCode')
      ->Join('test_categories', 'radiology_test_details.test_cat_id', '=', 'test_categories.id')
      ->Join('mri_tests', 'radiology_test_details.test', '=', 'mri_tests.id')
   ->select('appointments.*','appointments.persontreated','test_categories.name as category',
      'radiology_test_details.*','radiology_test_details.id as rtdid','facility_test.secondname',
      'mri_tests.name as tstname','mri_tests.technique','mri_tests.id as mriid','facilities.FacilityName','facility_test.firstname')
       ->where([['radiology_test_details.id', '=',$rtd_id],])
       ->first();

       $patientD=DB::table('appointments')
     ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
     ->leftjoin('dependant','appointments.persontreated','=','dependant.id')
     ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
       ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
       ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
         'dependant.firstName as dep1name','dependant.secondName as dep2name','dependant.gender as depgender',
         'dependant.dob as depdob','facilities.FacilityName','facilities.set_up','patient_admitted.condition'
         )
       ->where('appointments.id',$appointment)
       ->get();
    return view('doctor.tests.reportmri')->with('tsts1',$tsts1)->with('patientD',$patientD);
     }

     public function ultrareports(Request $request)
     {
      $appointment=$request->appointment_id;
      $rtd_id = $request->rtd_id;

       $tsts1 = DB::table('radiology_test_details')
       ->Join('appointments', 'radiology_test_details.appointment_id', '=', 'appointments.id')
      ->Join('patient_test', 'radiology_test_details.patient_test_id', '=', 'patient_test.id')
     ->Join('facility_test', 'radiology_test_details.user_id', '=', 'facility_test.user_id')
       ->Join('facilities', 'facility_test.facilitycode', '=', 'facilities.FacilityCode')
      ->Join('test_categories', 'radiology_test_details.test_cat_id', '=', 'test_categories.id')
      ->Join('ultrasound', 'radiology_test_details.test', '=', 'ultrasound.id')
     ->select('appointments.*','appointments.persontreated','test_categories.name as category',
      'radiology_test_details.*','radiology_test_details.id as rtdid','facility_test.secondname',
      'ultrasound.name as tstname','ultrasound.technique','ultrasound.id as ultraid','facilities.FacilityName','facility_test.firstname')
     ->where('radiology_test_details.id', '=',$rtd_id)
     ->first();

       $patientD=DB::table('appointments')
       ->leftjoin('triage_details','appointments.id','=','triage_details.appointment_id')
      ->leftjoin('triage_infants','appointments.id','=','triage_infants.appointment_id')
    ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
     ->leftjoin('dependant','appointments.persontreated','=','dependant.id')
     ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
       ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
       ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
         'dependant.firstName as dep1name','dependant.secondName as dep2name','dependant.gender as depgender',
         'dependant.dob as depdob','facilities.FacilityName','facilities.set_up','patient_admitted.condition',
         'triage_details.lmp as almp','triage_details.pregnant as apregnant','triage_infants.lmp as dlmp','triage_infants.pregnant as dpregnant')
       ->where('appointments.id',$appointment)
       ->get();
    return view('doctor.tests.reportultra')->with('tsts1',$tsts1)->with('patientD',$patientD);
     }

     public function testdetails($id)
     {

    $pdetails = DB::table('patient_test')
    ->leftJoin('appointments', 'patient_test.appointment_id', '=', 'appointments.id')
    ->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
    ->Join('doctors', 'appointments.doc_id', '=', 'doctors.id')
    ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
    ->select('appointments.persontreated','appointments.afya_user_id','appointments.id as appId',
    'doctors.name as docname','facilities.FacilityName','patient_admitted.condition')
    ->where('patient_test.id', '=',$id)
    ->first();

  $tsts = DB::table('patient_test')
  ->leftJoin('appointments', 'patient_test.appointment_id', '=', 'appointments.id')
  ->leftJoin('patient_test_details', 'patient_test.id', '=', 'patient_test_details.patient_test_id')
  ->leftJoin('icd10_option', 'patient_test_details.conditional_diag_id', '=', 'icd10_option.id')
  ->leftJoin('tests', 'patient_test_details.tests_reccommended', '=', 'tests.id')
  ->leftJoin('test_subcategories', 'tests.sub_categories_id', '=', 'test_subcategories.id')
  ->leftJoin('test_categories', 'test_subcategories.categories_id', '=', 'test_categories.id')
  ->select('tests.name as tname','test_subcategories.name as tsname','icd10_option.name as dname','patient_test_details.created_at as date',
  'patient_test_details.id as patTdid','test_categories.name as tcname','patient_test_details.done',
  'patient_test_details.tests_reccommended','appointments.id as AppId')
  ->where('patient_test.id', '=',$id)
  ->get();

           $rady = DB::table('patient_test')
               ->leftJoin('appointments', 'patient_test.appointment_id', '=', 'appointments.id')
               ->Join('radiology_test_details', 'patient_test.appointment_id', '=', 'radiology_test_details.appointment_id')
               ->leftJoin('test_categories', 'radiology_test_details.test_cat_id', '=', 'test_categories.id')
               ->select('radiology_test_details.created_at as date','radiology_test_details.test',
               'radiology_test_details.clinicalinfo','radiology_test_details.test_cat_id','radiology_test_details.done',
               'radiology_test_details.id as patTdid','test_categories.name as tcname')
                ->where('patient_test.id', '=',$id)
                ->get();
       return view('doctor.tests.tstdetails')->with('tsts',$tsts)->with('pdetails',$pdetails)->with('rady',$rady);
     }
    //  public function imgrslt($id)
    //  {
     //
    //    return view('doctor.tests.result');
     //
    //  }


}
