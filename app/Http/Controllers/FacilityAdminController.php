<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Redirect;
use App\Doctor;
use App\Testprice;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Auth;



class FacilityAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('facilityadmin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function facilityregister(){
        return view('facilityadmin.register');
    }
    public function addfacilityregister(){
        return view('facilityadmin.addregister');
    }
    public function addregistrar(Request $request){

    }

    public function store(Request $request)
    {
        $name=$request->name;
        $email=$request->email;
        $role=$request->role;
        $regno=$request->regno;
        $password=bcrypt($request->password);
        $facility=$request->facility;

        $userid=DB::table('users')->insertGetId([
            'name'=>$name,
            'email'=>$email,
            'role'=>$role,
            'password'=>$password,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('facility_registrar')->insert([
            'user_id'=>$userid,
            'regno'=>$regno,
            'facilitycode'=>$facility,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('role_user')->insert([
            'user_id'=>$userid,
            'role_id'=>9
            ]);

        return  Redirect()->action('FacilityAdminController@facilityregister');
    }

    public function facilitynurse(){

        return view('facilityadmin.nurse');
    }

public function storenurse(Request $request){

    $name=$request->name;
        $email=$request->email;
        $role=$request->role;
        $regno=$request->regno;
        $password=bcrypt($request->password);
        $facility=$request->facility;

        $userid=DB::table('users')->insertGetId([
            'name'=>$name,
            'email'=>$email,
            'role'=>$role,
            'password'=>$password,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('facility_nurse')->insert([
            'user_id'=>$userid,
            'regno'=>$regno,
            'facilitycode'=>$facility,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('role_user')->insert([
            'user_id'=>$userid,
            'role_id'=>4
            ]);

        return  Redirect()->action('FacilityAdminController@facilitynurse');
}
 public function facilitydoctor(){

    return view('facilityadmin.doctor');
 }

 public function laboratory(){

    return view('facilityadmin.lab');
 }

public function storelabtech(Request $request){
        $name=$request->name;
        $email=$request->email;
        $role=$request->role;
        $password=bcrypt($request->password);
        $facility=$request->facility;


        $userid=DB::table('users')->insertGetId([
            'name'=>$name,
            'email'=>$email,
            'role'=>$role,
            'password'=>$password,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('facility_test')->insert([
            'user_id'=>$userid,
            'firstname'=>$request->get('firstname'),
            'secondname'=>$request->get('lastname'),
            'address'=>$request->get('address'),
            'phone'=>$request->get('phone'),
            'department'=>$request->get('department'),
            'speciality'=>$request->get('speciality'),
            'qualification'=>$request->get('qualification'),
            'facilitycode'=>$facility,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('role_user')->insert([
            'user_id'=>$userid,
            'role_id'=>7  ]);


        return  Redirect()->action('FacilityAdminController@laboratory');


 }
 public function storedoctor(Request $request){
        $name=$request->name;
        $email=$request->email;
        $role=$request->role;
        $password=bcrypt($request->password);
        $facility=$request->facility;
        $doc=$request->doc;
        $userid=DB::table('users')->insertGetId([
            'name'=>$name,
            'email'=>$email,
            'role'=>$role,
            'password'=>$password,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('facility_doctor')->insert([
            'user_id'=>$userid,
            'doctor_id'=>$doc,
            'facilitycode'=>$facility,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('role_user')->insert([
            'user_id'=>$userid,
            'role_id'=>2
            ]);
        DB::table('doctors')->where('id',$doc)
         ->limit(1)->update([
            'user_id'=>$userid]);

        return  Redirect()->action('FacilityAdminController@facilitydoctor');


 }

 public function facilityofficer(){

    return view('facilityadmin.officers');
 }

 public function consltfee(){
   $data['fac_fee'] = DB::table('facility_admin')
   ->join('consultation_fees', 'facility_admin.facilitycode', '=', 'consultation_fees.facility_code')
   ->select('consultation_fees.old_consultation_fee as old', 'consultation_fees.new_consultation_fee as new','consultation_fees.medical_report_fee')
   ->where('facility_admin.user_id', Auth::user()->id)
   ->first();
    return view('facilityadmin.consltfee',$data);
 }

 public function storeofficer(Request $request){

     $name=$request->name;
        $email=$request->email;
        $role=$request->role;
        $regno=$request->regno;
        $regdate=$request->regdate;
        $address=$request->address;
        $qualify=$request->qualify;
        $speciality=$request->speciality;
        $sub=$request->sub_speciality;
        $password=bcrypt($request->password);
        $facility=$request->facility;

        $userid=DB::table('users')->insertGetId([
            'name'=>$name,
            'email'=>$email,
            'role'=>$role,
            'password'=>$password,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('facility_officer')->insert([
            'user_id'=>$userid,
            'regno'=>$regno,
            'facilitycode'=>$facility,
            'regdate'=>$regdate,
            'address'=>$address,
            'qualification'=>$qualify,
            'speciality'=>$speciality,
            'sub_speciality'=>$sub,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        DB::table('role_user')->insert([
            'user_id'=>$userid,
            'role_id'=>2
            ]);

        return  Redirect()->action('FacilityAdminController@facilityofficer');

 }

 public function setfees(Request $request){


        $new=$request->new;
        $old=$request->old;
        $med_report =$request->med_report;
        $fac = DB::table('facility_admin')
        ->leftJoin('consultation_fees', 'facility_admin.facilitycode', '=', 'consultation_fees.facility_code')
        ->select('facility_admin.facilitycode','consultation_fees.facility_code')
        ->where('facility_admin.user_id', Auth::user()->id)->first();

        $facility=$fac->facilitycode;
        $feecode=$fac->facility_code;


if($feecode){
  DB::table('consultation_fees')->where('facility_code',$feecode)
  ->update([
    'new_consultation_fee'=>$new,
    'old_consultation_fee'=>$old,
    'medical_report_fee'=>$med_report,
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
]);
}else{
$userid=DB::table('consultation_fees')->insert([
'facility_code'=>$facility,
'new_consultation_fee'=>$new,
'old_consultation_fee'=>$old,
'medical_report_fee'=>$med_report,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
]);
}
$data['fac_fee'] = DB::table('facility_admin')
->join('consultation_fees', 'facility_admin.facilitycode', '=', 'consultation_fees.facility_code')
->select('consultation_fees.old_consultation_fee as old', 'consultation_fees.new_consultation_fee as new')->where('facility_admin.user_id', Auth::user()->id)
->first();

return  Redirect()->action('FacilityAdminController@consltfee');
 }


 public function createdoc(){
  return view('facilityadmin.createdoc');
 }

public function finddoc(Request $request){

      $term = trim($request->q);
      if (empty($term)) {
           return \Response::json([]);
         }
       $drugs = Doctor::search($term)->limit(20)->get();
         $formatted_drugs = [];
          foreach ($drugs as $drug) {
             $formatted_drugs[] = ['id' => $drug->id, 'text' => $drug->name];
         }
     return \Response::json($formatted_drugs);
}

 /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function labtech($id)
     {
$labtechs=DB::table('facility_test')->where('user_id',$id)
->first();
       return view('facilityadmin.labtechupdate')->with('labtechs',$labtechs);
   }
  public function uplabtech(Request $request){


    $userid = $request->user_id;
    $address = $request->address;
    $phone = $request->phone;
    $department = $request->department;
    $speciality = $request->speciality;
    $qualification = $request->qualifications;
  DB::table('facility_test')
            ->where('user_id',$userid)
            ->update(['department'=>$department,
            'address'=>$address,
            'phone'=>$phone,
                  ]);

return  Redirect()->action('FacilityAdminController@laboratory');
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroylabtech($id)
    {
      DB::table("facility_test")->where('user_id',$id)->delete();
      DB::table("users")->where('id',$id)->delete();
      DB::table("role_user")->where('user_id',$id)->delete();
   return  Redirect()->action('FacilityAdminController@laboratory')
    ->with('success','Record deleted successfully');
         }
         public function testranges()
         {
$testranges=DB::table('tests')
->leftjoin('test_ranges','tests.id','=','test_ranges.tests_id')
->leftjoin('test_subcategories','tests.sub_categories_id','=','test_subcategories.id')
->leftjoin('test_machines','test_ranges.machine_id','=','test_machines.id')
->select('test_ranges.*','tests.name as tname','test_machines.id as machine_id','test_machines.name as machine',
'tests.id as testId','test_subcategories.name as subname')
->get();
return view('facilityadmin.testranges')->with('testranges',$testranges);
        }
        public function rangesadd(Request $request){
               $machine_id=$request->machine_name;
               $test_id=$request->test_id;
               $facility_id=$request->facility_id;
               $low_male=$request->low_male;
               $high_male=$request->high_male;
               $low_female=$request->low_female;
               $high_female=$request->high_female;
               $units=$request->units;
        $rangeId = DB::table('test_ranges')->insertGetId([
                   'tests_id'=>$test_id,
                   'machine_id'=>$machine_id,
                   'facility_id'=>$facility_id,
                   'low_male'=>$low_male,
                   'high_male'=>$high_male,
                   'low_female'=>$low_female,
                   'high_female'=>$high_female,
                   'units'=>$units,
                   'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                   'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
                   ]);

                   $data=DB::table('tests')
                   ->leftjoin('test_ranges','tests.id','=','test_ranges.tests_id')
                   ->leftjoin('test_subcategories','tests.sub_categories_id','=','test_subcategories.id')
                   ->leftjoin('test_machines','test_ranges.machine_id','=','test_machines.id')
                   ->select('test_ranges.*','tests.name as tname','test_machines.id as machine_id','test_machines.name as machine',
                   'tests.id as testId','test_subcategories.name as subname')
                   ->where('test_ranges.id',$rangeId)
                   ->first();
                   return response()->json($data);

  }

  public function destroyranges($id)
  {
DB::table("test_ranges")->where('id',$id)->delete();
return  Redirect()->action('FacilityAdminController@testranges');
 }

     public function updateranges(Request $request)
     {
     $t_ranges = $request->tranges_id;
       $units = $request->unit;
       $low_female = $request->low_female;
       $high_female = $request->high_female;
       $low_male = $request->low_male;
       $high_male = $request->high_male;
       $machine_id = $request->machine_id;

     DB::table('test_ranges')
               ->where('id',$t_ranges)
               ->update(['units'=>$units,
               'low_female'=>$low_female,
               'high_female'=>$high_female,
               'low_male'=>$low_male,
               'high_male'=>$high_male,
               'machine_id'=>$machine_id,
               ]);
               $data=DB::table('tests')
               ->leftjoin('test_ranges','tests.id','=','test_ranges.tests_id')
               ->leftjoin('test_subcategories','tests.sub_categories_id','=','test_subcategories.id')
               ->leftjoin('test_machines','test_ranges.machine_id','=','test_machines.id')
               ->select('test_ranges.*','tests.name as tname','test_machines.id as machine_id','test_machines.name as machine',
               'tests.id as testId','test_subcategories.name as subname')
               ->where('test_ranges.id',$t_ranges)
               ->first();
               return response()->json($data);

  // return view('admin.testranges');
     }





}
