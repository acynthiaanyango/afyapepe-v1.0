<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Appointment;
use App\Facility;
use App\Facility_doctor;
use App\Afya_user;
use Auth;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data['appointments']=Appointment::where('status','=',0)
                                           ->where('facility_id','=',Controller::doctor_reg_facility()->FacilityCode)
                                           ->get();
       $data['facility'] = Controller::doctor_reg_facility();
       $data['doctors']=Facility_doctor::select('doctors.*')
                                        ->where('facilitycode','=',Controller::doctor_reg_facility()->FacilityCode)
                                        ->join('doctors','doctors.id','=','facility_doctor.doctor_id')
                                        ->get();

       return view('appointments.calendar',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Appointment::create(['afya_user_id'=>$request->afya_user_id, 'persontreated'=>$request->persontreated, 'appointment_date'=>$request->appointment_date, 'appointment_time'=>$request->appointment_time, 'doc_id'=>$request->doc_id, 'facility_id'=>Controller::doctor_reg_facility()->FacilityCode, 'created_by_users_id'=>Auth::user()->id]);

        $subject="Appointment";
        $msg="Your appointment with ".Controller::doctor($request->doc_id)->name." for ".date('d M Y',strtotime($request->appointment_date)). " at ".$request->appointment_time." has been booked. Please avail yourself.
        Thank you!";

        $to=$request->email;
        $from='info@afyapepe.com';
        $from_name="AFyapepe";

        Controller::send_mail($from,$from_name,$to,$subject,$msg);

        return redirect('calendar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return redirect('calendar?afya_user_id='.$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment )
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        $appointment=Appointment::find($id);
        $appointment->update(['afya_user_id'=>$request->afya_user_id, 'persontreated'=>$request->persontreated, 'appointment_date'=>$request->appointment_date, 'appointment_time'=>$request->appointment_time, 'doc_id'=>$request->doc_id, 'facility_id'=>Controller::doctor_reg_facility()->FacilityCode, 'created_by_users_id'=>Auth::user()->id]);

        $subject="Appointment";
        $msg="Your Appointment with ".Controller::doctor($request->doc_id)->name." for ".date('d M Y',strtotime($request->appointment_date)). " at ".$request->appointment_time." has been booked";

        $to=$request->email;
        $from='info@afyapepe.com';
        $from_name="AFyapepe";

        Controller::send_mail($from,$from_name,$to,$subject,$msg);

        return redirect('calendar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }
}
