<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Patienttest;
use Illuminate\Support\Facades\Input;
use Auth;
use Carbon\Carbon;

class TestsaveDocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function store(Request $request)
     {

          $appointment=$request->appointment_id;
          $test_id=$request->test_id;
          $docnote=$request->docnote;

      $pttids= Patienttest::where('appointment_id',$appointment)
       ->first();

          if (is_null($pttids)) {
     $PatientTest = Patienttest ::create([
       'appointment_id' => $appointment,
       'test_status' => 0,
     ]);
         $ptid = $PatientTest->id;
          } else {
          // Already test exist - just get the id
           $ptid =$pttids->id;
           DB::table('patient_test')->where('id', $ptid)
           ->update(['test_status' => 0,]);
          }
          DB::table('appointments')->where('id', $appointment)
          ->update(['p_status' => 11,]);

          // Inserting  tests
     $patient_td = DB::table('patient_test_details')->insertGetId([
                       'patient_test_id' => $ptid,
                       'appointment_id' => $appointment,
                       'tests_reccommended' => $test_id,
                       'done' => 0,
                    ]);
        // Inserting patientNotes tests
     if ($docnote) {
     $patientNotes = DB::table('patientNotes')->insert([
         'appointment_id' => $appointment,
         'note' => $docnote,
         'target' => 'Test',
         'ptd_id' => $patient_td,
          ]);
     }


     return redirect()->action('PatientTestController@testdata', ['id' =>  $appointment]);

           }
           public function destroytest($id)
           {
             $pttd=DB::table('patient_test_details')
             ->where('id',$id)
             ->first();

             DB::table("patient_test_details")->where('id',$id)->update(array('deleted'=>1));

       return redirect()->action('PatientTestController@testdata', ['id' => $pttd->appointment_id]);

       }




public function otherimagingPost(Request $request)
{
$now = Carbon::now();

    $appointment=$request->appointment;
    $test=$request->test;
    $clinical=$request->clinical;
    $cat_id=$request->cat_id;
    $target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
  ->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
  'appointment_id' => $appointment,
  'test_status' => 0,
  'created_at' =>$now,
  'updated_at' => $now,
]);
    $ptid = $PatientTest->id;
     } else {
     // Already test exist - just get the id
      $ptid =$pttids->id;
      DB::table('patient_test')->where('id', $ptid)
      ->update(['test_status' => 0,]);
     }
     DB::table('appointments')->where('id', $appointment)
     ->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
                 'patient_test_id' => $ptid,
                 'appointment_id' => $appointment,
                 'clinicalinfo' => $clinical,
                 'test_cat_id' => $cat_id,
                 'target' => $target,
                 'test' => $test,
                 'done' => 0,
                 'user_id' =>$Uid1,
                 'confirm' => 'N',
                 'created_at' => $now,
              ]);
return redirect()->action('PatientTestController@testesImage',[$appointment]);

}

public function Otherremove(Request $request)
{
  $now = Carbon::now();
  $test=$request->test;
  $cat_id=$request->cat_id;
  $appointment =$request->appointment;
  $id1 = Auth::user()->id;
  DB::table("radiology_test_details")->where('id',$test)
  ->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);
  return redirect()->action('PatientTestController@testesImage',[$appointment]);

}


public function mriPost(Request $request)
{
$now = Carbon::now();

$appointment=$request->appointment;
$test=$request->test;
$clinical=$request->clinical;
$cat_id=$request->cat_id;
$target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
'appointment_id' => $appointment,
'test_status' => 0,
'created_at' =>$now,
'updated_at' => $now,
]);
$ptid = $PatientTest->id;
} else {
// Already test exist - just get the id
$ptid =$pttids->id;
DB::table('patient_test')->where('id', $ptid)
->update(['test_status' => 0,]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
          'patient_test_id' => $ptid,
          'appointment_id' => $appointment,
          'clinicalinfo' => $clinical,
          'test_cat_id' => $cat_id,
          'target' => $target,
          'test' => $test,
          'done' => 0,
          'user_id' =>$Uid1,
          'confirm' => 'N',
          'created_at' => $now,
       ]);
return redirect()->action('PatientTestController@testdatamri',[$appointment]);

}

public function mriTestremove(Request $request)
{
$now = Carbon::now();
$test=$request->test;
$cat_id=$request->cat_id;
$appointment =$request->appointment;
$id1 = Auth::user()->id;
DB::table("radiology_test_details")->where('id',$test)
->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);
return redirect()->action('PatientTestController@testdatamri',[$appointment]);

}




public function ctTest(Request $request)
{
$now = Carbon::now();

$appointment=$request->appointment;
$test=$request->test;
$clinical=$request->clinical;
$cat_id=$request->cat_id;
$target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
'appointment_id' => $appointment,
'test_status' => 0,
'created_at' =>$now,
'updated_at' => $now,
]);
$ptid = $PatientTest->id;
} else {
// Already test exist - just get the id
$ptid =$pttids->id;
DB::table('patient_test')->where('id', $ptid)
->update(['test_status' => 0,]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
          'patient_test_id' => $ptid,
          'appointment_id' => $appointment,
          'clinicalinfo' => $clinical,
          'test_cat_id' => $cat_id,
          'target' => $target,
          'test' => $test,
          'done' => 0,
          'user_id' =>$Uid1,
          'confirm' => 'N',
          'created_at' => $now,
       ]);
return redirect()->action('PatientTestController@testsImaging',[$appointment]);

}

public function ctTestremove(Request $request)
{
$now = Carbon::now();
$test=$request->test;
$cat_id=$request->cat_id;
$appointment =$request->appointment;
$id1 = Auth::user()->id;
DB::table("radiology_test_details")->where('id',$test)
->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);

return redirect()->action('PatientTestController@testsImaging',[$appointment]);

}


public function ultraTest(Request $request)
{
$now = Carbon::now();

$appointment=$request->appointment;
$test=$request->test;
$clinical=$request->clinical;
$cat_id=$request->cat_id;
$target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
'appointment_id' => $appointment,
'test_status' => 0,
'created_at' =>$now,
'updated_at' => $now,
]);
$ptid = $PatientTest->id;
} else {
// Already test exist - just get the id
$ptid =$pttids->id;
DB::table('patient_test')->where('id', $ptid)
->update(['test_status' => 0,]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
          'patient_test_id' => $ptid,
          'appointment_id' => $appointment,
          'clinicalinfo' => $clinical,
          'test_cat_id' => $cat_id,
          'target' => $target,
          'test' => $test,
          'done' => 0,
          'user_id' =>$Uid1,
          'confirm' => 'N',
          'created_at' => $now,
       ]);
return redirect()->action('PatientTestController@testdataultra',[$appointment]);

}

public function ultraTestremove(Request $request)
{
$now = Carbon::now();
$test=$request->test;
$cat_id=$request->cat_id;
$appointment =$request->appointment;
$id1 = Auth::user()->id;
DB::table("radiology_test_details")->where('id',$test)
->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);

return redirect()->action('PatientTestController@testdataultra',[$appointment]);

}


public function xrayTest(Request $request)
{
$now = Carbon::now();

$appointment=$request->appointment;
$test=$request->test;
$clinical=$request->clinical;
$cat_id=$request->cat_id;
$target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
'appointment_id' => $appointment,
'test_status' => 0,
'created_at' =>$now,
'updated_at' => $now,
]);
$ptid = $PatientTest->id;
} else {
// Already test exist - just get the id
$ptid =$pttids->id;
DB::table('patient_test')->where('id', $ptid)
->update(['test_status' => 0,]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
          'patient_test_id' => $ptid,
          'appointment_id' => $appointment,
          'clinicalinfo' => $clinical,
          'test_cat_id' => $cat_id,
          'target' => $target,
          'test' => $test,
          'done' => 0,
          'user_id' =>$Uid1,
          'confirm' => 'N',
          'created_at' => $now,
       ]);
return redirect()->action('PatientTestController@testdataxray',[$appointment]);

}

public function xrayTestremove(Request $request)
{
$now = Carbon::now();
$test=$request->test;
$cat_id=$request->cat_id;
$appointment =$request->appointment;
$id1 = Auth::user()->id;
DB::table("radiology_test_details")->where('id',$test)
->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);

return redirect()->action('PatientTestController@testdataxray',[$appointment]);

}

public function imagingdestroytest($id)
{
$pttd=DB::table('radiology_test_details')
->where('id',$id)
->first();
DB::table("radiology_test_details")->where('id',$id)
->update(['deleted' =>1]);

return redirect()->action('PatientTestController@test_all', ['id' => $pttd->appointment_id]);

}
public function summPatients(Request $request)
{

  $appointment=$request->appointment_id;
  $med =$request->current_med;
  $doc_note =$request->doc_note;
  $fam_note =$request->fam_note;
  $afya_user_id =$request->afya_user_id;

  $now = Carbon::now();
       if($doc_note){
         $psummary = DB::table('patient_summary')->where('appointment_id',$appointment)->first();
       if($psummary){
          DB::table('patient_summary')->where('appointment_id',$appointment)->update([
            'notes' => $doc_note,
            'updated_at' => $now,
         ]);
       }else{
        $patienttd = DB::table('patient_summary')->insert([
                 'appointment_id' => $appointment,
                 'notes' => $doc_note,
                 'created_at' => $now,
                 'updated_at' => $now,
              ]);
             }
           }
             if($fam_note){
               $pfam = DB::table('family_summary')->where('appointment_id',$appointment)->first();
             if($pfam){
                DB::table('family_summary')->where('appointment_id',$appointment)->update([
                  'notes' => $fam_note,
                  'updated_at' => $now,
               ]);
             }else{
              $patienttd = DB::table('family_summary')->insert([
                       'appointment_id' => $appointment,
                       'notes' => $fam_note,
                       'afya_user_id' => $afya_user_id,
                       'created_at' => $now,
                       'updated_at' => $now,
                    ]);
                   }
                 }
   if($med){
     $pmed = DB::table('current_medication')->where('appointment_id',$appointment)->first();
   if($pmed){
      DB::table('current_medication')->where('appointment_id',$appointment)->update([
        'drugs' => $med,
        'updated_at' => $now,
     ]);
   }else{
    $medds = DB::table('current_medication')->insert([
             'appointment_id' => $appointment,
             'drugs' => $med,
             'created_at' => $now,
             'updated_at' => $now,
          ]);
         }
}
return redirect()->action('PatientController@examination', ['id' =>  $appointment]);
}

public function trgpost(Request $request)

{
  $appointment = $request->appointment_id;
  $id = $request->id;
  $weight = $request->weight;
  $heightS = $request->current_height;
  $temperature = $request->temperature;
  $systolic = $request->systolic;
  $diastolic = $request->diastolic;
  $allergies = $request->allergies;
  $chiefcompliant = $request->chiefcomplaint;
  $observation = $request->observation;
  $symptoms = $request->symptoms;
  $nurse = $request->nurse;
  $doctor = $request->doctor;
  $lmp = $request->lmp;
  $rbs = $request->rbs;
  $hr = $request->hr;
  $rr = $request->rr;
  $pregnant = $request->pregnant;

        if($chiefcompliant){ $chiefcompliant = implode(',', $chiefcompliant); }
                if($symptoms){$symptoms= implode(',', $symptoms); }
                if($observation){$observation= implode(',', $observation); }

$ptriage = DB::table('triage_details')->where('appointment_id',$appointment)->first();
if($ptriage){
  DB::table('triage_details')->where('appointment_id',$appointment)->update([
    'current_weight'=> $weight,
    'current_height'=>$heightS,
    'temperature'=>$temperature,
    'systolic_bp'=>$systolic,
    'diastolic_bp'=>$diastolic,
    'chief_compliant'=>$chiefcompliant,
    'observation'=>$observation,
    'symptoms'=>$symptoms,
    'nurse_notes'=>'',
    'pregnant'=>$pregnant,
    'lmp'=>$lmp,
    'Doctor_note'=>$nurse,
    'prescription'=>'',
    'rbs'=>$rbs,
    'hr'=>$hr,
    'rr'=>$rr,
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
  ]);

}else{
                DB::table('triage_details')->insert([
                  'appointment_id' => $appointment,
                  'current_weight'=> $weight,
                  'current_height'=>$heightS,
                  'temperature'=>$temperature,
                  'systolic_bp'=>$systolic,
                  'diastolic_bp'=>$diastolic,
                  'chief_compliant'=>$chiefcompliant,
                  'observation'=>$observation,
                  'symptoms'=>$symptoms,
                  'nurse_notes'=>'',
                  'pregnant'=>$pregnant,
                  'lmp'=>$lmp,
                  'Doctor_note'=>$nurse,
                  'prescription'=>'',
                  'rbs'=>$rbs,
                  'hr'=>$hr,
                  'rr'=>$rr,
                  'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                  'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]

                );
              }
                DB::table('appointments')->where('id',$appointment)->update([
                  'status'=>2,
                ]);


                return redirect()->action('PatientController@examination', ['id' =>  $appointment]);
  }

public function ImpressionSave(Request $request)
{
  $appointment=$request->appointment_id;
  $doc_note =$request->doc_note;
  $now = Carbon::now();
if($doc_note){
$ge = DB::table('impression')->where('appointment_id',$appointment)->first();
if($ge){
$patienttd = DB::table('impression')->where('appointment_id',$appointment)->update([
'notes' => $doc_note,
'updated_at' => $now,
]);
}else{
$patienttd = DB::table('impression')->insert([
'appointment_id' => $appointment,
'notes' => $doc_note,
'created_at' => $now,
'updated_at' => $now,
]);
}

}

return redirect()->action('PatientTestController@alltestdata', ['id' =>  $appointment]);
}
}
