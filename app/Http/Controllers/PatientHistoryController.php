<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Smokinghistory;
use App\Alcoholhistory;
use App\Medicalhistory;
use App\Surgicalprocedures;
use App\Medhistory;
use App\Druglist;


class PatientHistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }
    public function show($id)
    {
      $today = Carbon::today();
      $today = $today->toDateString();
$facilitycode=DB::table('facility_nurse')->where('user_id', Auth::id())->first();

$data['patient'] = DB::table('appointments')
->join('afya_users','appointments.afya_user_id','=','afya_users.id')
->select('appointments.id as appid','afya_users.*')
->where([['appointments.status','=',1],['appointments.facility_id',$facilitycode->facilitycode],
['afya_users.id',$id],])
->whereDate('appointments.created_at','=',$today)->first();

$data['vaccines'] = DB::table('vaccine')->get();
    $data['smoking']=Smokinghistory::where('afya_user_id','=',$id)->first();
    $data['alcohol']=Alcoholhistory::where('afya_user_id','=',$id)->first();
    $data['medical']=Medicalhistory::where('afya_user_id','=',$id)->first();
    $data['drugs']=Druglist::all();

    return view('nurse.procedures',$data);
    }


    public function store(Request $request)
    {
      $id=$request->afya_user_id;
      $smoking_id=$request->smoking_id;
      $alcohol_id=$request->alcohol_id;
      $medical_id =$request->medical_id;
      $name_of_surgery =$request->name_of_surgery;
      $drug_id =$request->drug_id;
      $appid=$request->appid;
      $chronics=$request->chronic;

      if($smoking_id) {
      $smokinghistory=Smokinghistory::find($smoking_id);
      $smokinghistory->update($request->all());
  }else{     Smokinghistory::create($request->all());    }

if($alcohol_id) {
      $alcoholhistory=Alcoholhistory::find($alcohol_id);
      $alcoholhistory->update($request->all());

  }else{ Alcoholhistory::create($request->all());  }

  if($medical_id) {
    $medicalhistory=Medicalhistory::find($medical_id);
    $medicalhistory->update($request->all());

    }else{ Medicalhistory::create($request->all());  }

if($name_of_surgery) {
Surgicalprocedures::create($request->all());
}


$med_date=$request->med_date;
$dosage=$request->dosage;
if($drug_id) {

DB::table('self_reported_medication')->insert([
'afya_user_id'=>$id,
'drug_id'=>$drug_id,
'dosage'=>$dosage,
'med_date'=>$med_date,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

}





if($chronics){
foreach($chronics as $key ) {
DB::table('patient_diagnosis')->insert([
'afya_user_id'=>$id,
'appointment_id'=>$appid,
'disease_id'=>$key,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
}

$drugs=$request->drugs;
if($drugs){
foreach($drugs as $key =>$drug) {
DB::table('afya_users_allergy')->insert([
'afya_user_id'=>$id,
'allergies_type_id'=>$drug,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
}
$foods=$request->foods;
if($foods){
foreach($foods as $key) {
DB::table('afya_users_allergy')->insert([
'afya_user_id'=>$id,
'allergies_type_id'=>$key,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
}
$latexs=$request->latexs;
if($latexs){
foreach($latexs as $key) {
DB::table('afya_users_allergy')->insert([
'afya_user_id'=>$id,
'allergies_type_id'=>$key,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
}
$molds=$request->molds;
if($molds){
foreach($molds as $key) {
DB::table('afya_users_allergy')->insert([
'afya_user_id'=>$id,

'allergies_type_id'=>$key,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
}
$pets=$request->pets;
if($pets)
{
foreach($pets as $key) {
DB::table('afya_users_allergy')->insert([
'afya_user_id'=>$id,
'allergies_type_id'=>$key,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
}

$pollens=$request->pollens;
if($pollens) {
foreach($pollens as $key) {
DB::table('afya_users_allergy')->insert([
'afya_user_id'=>$id,
'allergies_type_id'=>$key,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
}
$insects=$request->insects;
if($insects){
foreach($insects as $key) {
DB::table('afya_users_allergy')->insert([
'afya_user_id'=>$id,
'allergies_type_id'=>$key,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
}
$disease_name=$request->disease_name;
$vac_name=$request->vac_name;
$vac_date=$request->vac_date;
if($disease_name){

DB::table('vaccination')->insert([
'userId'=>$id,
'diseaseId'=>$disease_name,
'vaccine_name'=>$vac_name,
'Yes'=>'yes',
'yesdate'=>$vac_date,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

}
return redirect()->action('NurseController@show',[$id]);

    }


    public function RegUpHist($id)
    {
      $today = Carbon::today();
      $today = $today->toDateString();
$facilitycode=DB::table('facility_registrar')->where('user_id', Auth::id())->first();

$data['patient'] = DB::table('afya_users')->where('afya_users.id',$id)->first();

$data['vaccines'] = DB::table('vaccine')->get();

    $data['smoking']=Smokinghistory::where('afya_user_id','=',$id)->first();
    $data['alcohol']=Alcoholhistory::where('afya_user_id','=',$id)->first();
    $data['medical']=Medicalhistory::where('afya_user_id','=',$id)->first();
    $data['drugs']=Druglist::all();

    return view('registrar.reghistdata',$data);
    }


        public function Reg_store(Request $request)
        {
          $id=$request->afya_user_id;
          $smoking_id=$request->smoking_id;
          $alcohol_id=$request->alcohol_id;
          $medical_id =$request->medical_id;
          $name_of_surgery =$request->name_of_surgery;
          $drug_id =$request->drug_id;
          $chronics=$request->chronic;

          if($smoking_id) {
          $smokinghistory=Smokinghistory::find($smoking_id);
          $smokinghistory->update($request->all());
      }else{     Smokinghistory::create($request->all());    }

    if($alcohol_id) {
          $alcoholhistory=Alcoholhistory::find($alcohol_id);
          $alcoholhistory->update($request->all());

      }else{ Alcoholhistory::create($request->all());  }

      if($medical_id) {
        $medicalhistory=Medicalhistory::find($medical_id);
        $medicalhistory->update($request->all());

        }else{ Medicalhistory::create($request->all());  }

    if($name_of_surgery) {
    Surgicalprocedures::create($request->all());
    }

    $med_date=$request->med_date;
    if($drug_id) {

    foreach($drug_id as $key =>$drugi) {
    DB::table('self_reported_medication')->insert([
    'afya_user_id'=>$id,
    'drug_id'=>$drug,
    'med_date'=>$med_date,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
    }
    }






    if($chronics){
    foreach($chronics as $key ) {
    DB::table('patient_diagnosis')->insert([
    'afya_user_id'=>$id,
    'disease_id'=>$key,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
    }
    }

    $drugs=$request->drugs;
    if($drugs){
    foreach($drugs as $key =>$drug) {
    DB::table('afya_users_allergy')->insert([
    'afya_user_id'=>$id,
    'allergies_type_id'=>$drug,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
    }
    }
    $foods=$request->foods;
    if($foods){
    foreach($foods as $key) {
    DB::table('afya_users_allergy')->insert([
    'afya_user_id'=>$id,
    'allergies_type_id'=>$key,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
    }
    }
    $latexs=$request->latexs;
    if($latexs){
    foreach($latexs as $key) {
    DB::table('afya_users_allergy')->insert([
    'afya_user_id'=>$id,
    'allergies_type_id'=>$key,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
    }
    }
    $molds=$request->molds;
    if($molds){
    foreach($molds as $key) {
    DB::table('afya_users_allergy')->insert([
    'afya_user_id'=>$id,

    'allergies_type_id'=>$key,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
    }
    }
    $pets=$request->pets;
    if($pets)
    {
    foreach($pets as $key) {
    DB::table('afya_users_allergy')->insert([
    'afya_user_id'=>$id,
    'allergies_type_id'=>$key,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
    }
    }

    $pollens=$request->pollens;
    if($pollens) {
    foreach($pollens as $key) {
    DB::table('afya_users_allergy')->insert([
    'afya_user_id'=>$id,
    'allergies_type_id'=>$key,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
    }
    }
    $insects=$request->insects;
    if($insects){
    foreach($insects as $key) {
    DB::table('afya_users_allergy')->insert([
    'afya_user_id'=>$id,
    'allergies_type_id'=>$key,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
    }
    }
    $disease_name=$request->disease_name;
    $vac_name=$request->vac_name;
    $vac_date=$request->vac_date;
    if($disease_name){

    DB::table('vaccination')->insert([
    'userId'=>$id,
    'diseaseId'=>$disease_name,
    'vaccine_name'=>$vac_name,
    'Yes'=>'yes',
    'yesdate'=>$vac_date,
    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

    }
    return redirect()->action('privateController@histdata',[$id]);

        }

        public function histdetails($id)
        {
$today = Carbon::today();
$today = $today->toDateString();
$facilitycode=DB::table('facility_doctor')->where('user_id', Auth::id())->first();

$patient = DB::table('appointments')
->join('afya_users','appointments.afya_user_id','=','afya_users.id')
->select('afya_users.*','appointments.id as appid')
->where('appointments.id',$id)->first();
$afyaid = $patient->id;


$data['vaccines'] = DB::table('vaccine')->get();
$data['smoking']=Smokinghistory::where('afya_user_id','=',$afyaid)->first();
$data['alcohol']=Alcoholhistory::where('afya_user_id','=',$afyaid)->first();
$data['medical']=Medicalhistory::where('afya_user_id','=',$afyaid)->first();
$data['drugs']=Druglist::all();
$data['tab1']='active'; $data['tab2']=''; $data['tab3']='';
$data['tab4']=''; $data['tab5']=''; $data['tab6']='';
return view('doctor.histdata',$data)->with('patient',$patient);
        }


public function Doc_smoking(Request $request)
{
  $id=$request->afya_user_id;
  $smoking_id=$request->smoking_id;
  $alcohol_id=$request->alcohol_id;
  $appid=$request->appointment_id;

  if($smoking_id) {
  $smokinghistory=Smokinghistory::find($smoking_id);
  $smokinghistory->update($request->all());
}else{     Smokinghistory::create($request->all());    }

if($alcohol_id) {
  $alcoholhistory=Alcoholhistory::find($alcohol_id);
  $alcoholhistory->update($request->all());

}else{ Alcoholhistory::create($request->all());  }

return redirect()->action('PatientHistoryController@histdetails',[$appid]);


}

public function Doc_medical(Request $request)
{
  $id=$request->afya_user_id;
  $appid=$request->appointment_id;
  $medical_id =$request->medical_id;

  if($medical_id) {
  $medicalhistory=Medicalhistory::find($medical_id);
  $medicalhistory->update($request->all());

  }else{ Medicalhistory::create($request->all());  }

  return redirect()->action('PatientHistoryController@histdetails',[$appid]);


}
public function Doc_surgical(Request $request)
{
  $id=$request->afya_user_id;
  $appid=$request->appointment_id;
  $name_of_surgery1 =$request->name_of_surgery1;
  $name_of_surgery2 =$request->name_of_surgery2;
  $surgery_date1 =$request->surgery_date1;
  $surgery_date2 =$request->surgery_date2;
  if($name_of_surgery1){
  DB::table('self_reported_surgical_procedures')->insert([
  'afya_user_id'=>$id,
  'name_of_surgery'=>$name_of_surgery1,
  'surgery_date'=>$surgery_date1,
  'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
  'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
  }
  if($name_of_surgery2){
  DB::table('self_reported_surgical_procedures')->insert([
  'afya_user_id'=>$id,
  'name_of_surgery'=>$name_of_surgery2,
  'surgery_date'=>$surgery_date2,
  'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
  'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
  }

$chronics1=$request->chronic1;
$chronics2=$request->chronic2;
$status1=$request->status1;
$status2=$request->status2;
if($chronics1){
DB::table('patient_diagnosis')->insert([
'afya_user_id'=>$id,
'disease_id'=>$chronics1,
'notes'=>$status1,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
if($chronics2){
DB::table('patient_diagnosis')->insert([
'afya_user_id'=>$id,
'disease_id'=>$chronics2,
'notes'=>$status2,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
return redirect()->action('PatientHistoryController@histdetails',[$appid]);


}

public function Doc_drug(Request $request)
{
  $id=$request->afya_user_id;
  $appid=$request->appointment_id;

$drug_id1 =$request->drug_id1;
$med_date1=$request->med_date1;
$dosage1=$request->dosage1;
if($drug_id1) {

DB::table('self_reported_medication')->insert([
'afya_user_id'=>$id,
'drug_id'=>$drug_id1,
'dosage'=>$dosage1,
'med_date'=>$med_date1,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

}
$drug_id2 =$request->drug_id2;
$med_date2=$request->med_date2;
$dosage2=$request->dosage2;
if($drug_id2) {

DB::table('self_reported_medication')->insert([
'afya_user_id'=>$id,
'drug_id'=>$drug_id2,
'dosage'=>$dosage2,
'med_date'=>$med_date2,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

}
$drug_id3 =$request->drug_id3;
$med_date3=$request->med_date3;
$dosage3=$request->dosage3;
if($drug_id3) {

DB::table('self_reported_medication')->insert([
'afya_user_id'=>$id,
'drug_id'=>$drug_id3,
'dosage'=>$dosage3,
'med_date'=>$med_date3,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

}
$drug_id4 =$request->drug_id4;
$med_date4=$request->med_date4;
$dosage4=$request->dosage4;
if($drug_id4) {

DB::table('self_reported_medication')->insert([
'afya_user_id'=>$id,
'drug_id'=>$drug_id4,
'dosage'=>$dosage4,
'med_date'=>$med_date4,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

}
$drug_id5 =$request->drug_id5;
$med_date5=$request->med_date5;
$dosage5=$request->dosage5;
if($drug_id5) {

DB::table('self_reported_medication')->insert([
'afya_user_id'=>$id,
'drug_id'=>$drug_id5,
'dosage'=>$dosage5,
'med_date'=>$med_date5,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

}
$drugs=$request->allergies;
if($drugs){
foreach($drugs as $key =>$drug) {
DB::table('afya_users_allergy')->insert([
'afya_user_id'=>$id,
'allergies_type_id'=>$drug,
'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
}
}


return redirect()->action('PatientHistoryController@histdetails',[$appid]);


}

public function Doc_vaccine(Request $request)
{
  $id=$request->afya_user_id;
  $appid=$request->appointment_id;
  $disease_name=$request->disease_name;
  $vac_name=$request->vac_name;
  $vac_date=$request->vac_date;

  if($disease_name){
  DB::table('vaccination')->insert([
  'userId'=>$id,
  'diseaseId'=>$disease_name,
  'vaccine_name'=>$vac_name,
  'Yes'=>'yes',
  'yesdate'=>$vac_date,
  'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
  'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

  }
  return redirect()->action('PatientHistoryController@histdetails',[$appid]);


}

public function Doc_abnormal(Request $request)
{
  $id=$request->afya_user_id;
  $appid=$request->appointment_id;
  $abnm1 =$request->abnm1;
  $abnm11 =$request->abnm11;
  $abnm2 =$request->abnm2;
  $abnm22 =$request->abnm22;
  $abnm3 =$request->abnm3;
  $abnm33 =$request->abnm33;
  $abnm4 =$request->abnm4;
  $abnm44 =$request->abnm44;

  if($abnm1){
  DB::table('patient_abnormalities')->insert([
  'afya_user_id'=>$id,
  'name'=>$abnm1,
  'notes'=>$abnm11,
  'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
  'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
  }
  if($abnm2){
  DB::table('patient_abnormalities')->insert([
  'afya_user_id'=>$id,
  'name'=>$abnm2,
  'notes'=>$abnm22,
  'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
  'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
  }
  if($abnm3){
  DB::table('patient_abnormalities')->insert([
  'afya_user_id'=>$id,
  'name'=>$abnm3,
  'notes'=>$abnm33,
  'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
  'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
  }
  if($abnm4){
  DB::table('patient_abnormalities')->insert([
  'afya_user_id'=>$id,
  'name'=>$abnm4,
  'notes'=>$abnm44,
  'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
  'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
  }
  return redirect()->action('PatientHistoryController@histdetails',[$appid]);
}

}
