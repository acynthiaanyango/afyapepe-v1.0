<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                   <!-- <span><img alt="user" class="img-circle" src="img/profile_small.jpg" /></span> -->
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                    </span> <span class="text-muted text-xs block">{{ Auth::user()->role }} <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Profile</a></li>
                        <li><a href="#">Contacts</a></li>
                        <li><a href="#">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/logout') }}">Logout</a></li>

                    </ul>
                </div>
                <div class="logo-element">
                    Afya+
                </div>
            </li>
  <?php $Userrole=DB::table('users')
  ->join('facility_admin','users.id','=','facility_admin.user_id')
  ->where('users.id', Auth::id())
  ->first(); ?>

@if($Userrole->department =='Test center')
<li><a href="{{ URL::to('laboratory') }}"><i class="fa fa-users"></i> <span>Facility Personnel</span></a></li>
<li><a href="{{ URL::to('testranges') }}"><i class="fa fa-arrows-h"></i> <span>Add Test Ranges</span></a></li>
@else



<!-- <li >
<a href="{{ URL::to('facilityadmin') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
</li> -->
<li><a href="{{ URL::to('facilityregister') }}"><i class="fa fa-users"></i> <span>Registrar</span></a></li>
<li><a href="{{ URL::to('facilitynurse') }}"><i class="fa fa-users"></i> <span>Nurse</span></a></li>
<li><a href="{{ URL::to('facilitydoctor') }}"><i class="fa fa-users"></i> <span>Doctor</span></a></li>
<li><a href="{{ URL::to('facilityofficer') }}"><i class="fa fa-users"></i> <span>C.Officers</span></a></li>
<li><a href="{{ URL::to('consltfee') }}"><i class="fa fa-money"></i> <span>Consultation Fee</span></a></li>
<li><a href="{{ URL::to('facility-finance') }}"><i class="fa fa-money"></i> <span>Finance</span></a></li>
@endif

<li class="active"><a href="#"><i class="fa fa-dollar"></i> <span class="nav-label">Tests Prices</span> <span class="fa arrow"></span></a>
</li>
<li><a href="{{ URL::to('testprices') }}"> <span>Laboratory Test Prices</span></a></li>
<li><a href="{{ URL::to('testpricesct') }}"><span>CT-Scan Prices</span></a></li>
<li><a href="{{ URL::to('testpricesxray') }}"><span>Xray Prices</span></a></li>
<li><a href="{{ URL::to('testpricesotherIm') }}"><span>Other Imaging Prices</span></a></li>

<li><a href="{{ URL::to('testpricesultra') }}"><span>Ultrasound Prices</span></a></li>
<li><a href="{{ URL::to('testpricesmri') }}"><span>MRI Prices</span></a></li>

<li><a href="{{ URL::to('upimages') }}"><span>Upload Images</span></a></li>

<li> <a href="{{ URL::to('#')}}">  <i class="fa fa-envelope "></i> <span>Email</span></a></li>
<li> <a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i><span>Logout</span></a></li>
  </ul>

    </div>
</nav>
