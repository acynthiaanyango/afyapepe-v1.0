  @extends('layouts.facilityadmin2')
        @section('title', 'Test Prices')
        @section('styles')

        @endsection
        @section('content')

<div class="container">
<div class="col-lg-11">
    <div class="ibox-title">
        <h5>Laboratory Test Price List</h5>
      </div>
       <div class="table-responsive ibox-content">
          <table class="table table-striped table-bordered table-hover dataTables-example" >
        <thead>
  					<tr>
  						<th>#</th>
  						<th>Name</th>
              <th>Sub category</th>
              <th>Availability</th>
              <th>Amount (KSH)</th>
  						<th>Actions</th>
              <!-- <th>Discounts</th> -->
  					</tr>
  				</thead>
          <?php  $i =1;?>
  				@foreach($data as $item)
          <?php
          $admin= DB::table('facility_admin')->where('user_id', '=', Auth::user()->id)
            ->select('facilitycode')->first();
        $facility_id= $admin->facilitycode;

          $paid =DB::table('test_price')
          ->where([['tests_id',$item->testId],['facility_id',$facility_id],])
          ->select('id','availability','amount')
          ->first();

           ?>
  				<tr class="item{{$item->testId}}">
  					<td>{{$item->testId}}</td>
  					<td>{{$item->name}}</td>
            <td>{{$item->sub}}</td>
            <td> @if($paid){{$paid->availability}}@endif</td>
            <td>@if($paid){{$paid->amount}}@endif</td>
  					<td>
           @if($paid)
              <button class="edit-modal btn btn-info" data-id="{{$paid->id}}"
  							data-name="{{$item->name}}" data-avala="{{$paid->availability}}"
                data-amount="{{$paid->amount}}">
  							<span class="glyphicon glyphicon-edit"></span> EDIT
  						</button>
              @else
              <button class="add-modal btn btn-primary" data-id="{{$item->testId}}"
    							data-name="{{$item->name}}" >
    							<span class="glyphicon glyphicon-plus"></span>ADD
    						</button>
                @endif
            </td>
            <!-- <td><a href="{{route('discounts',$item->testId)}}">Discount</a></td> -->
  				</tr>
          <?php $i++; ?>
  				@endforeach
  			</table>
  		</div>
    </div>

  	</div>

  	<div id="myModal" class="modal fade" role="dialog">
  		<div class="modal-dialog">
  			<!-- Modal content-->
  			<div class="modal-content">
  				<div class="modal-header">
  					<button type="button" class="close" data-dismiss="modal">&times;</button>
  					<h4 class="modal-title"></h4>
  				</div>
  				<div class="modal-body">
  					<form class="form-horizontal" role="form">
  						<div class="form-group">
  							<!-- <label class="control-label col-sm-2" for="id">ID:</label> -->
  							<div class="col-sm-10">
  								<input type="hidden" class="form-control" id="fid" name="tests_id" >
                </div>
  						</div>

  						<div class="form-group">
  							<label class="control-label col-sm-2" for="name">Test Name:</label>
  							<div class="col-sm-10">
  								<input type="text" class="form-control" id="n" disabled>

  							</div>
  						</div>
              <div class="form-group">
  							<label class="control-label col-sm-2" for="availability">Availability:</label>
  							<div class="col-sm-10">
                  <select class="form-control availability" id="avail" name="availability">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                  </select>
               </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2">Charges(KSH):</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="amnt" name="amount">
                </div>
              </div>
     {{ csrf_field() }}


  					</form>

  					<div class="modal-footer">
  						<button type="button" class="btn actionBtn" data-dismiss="modal">
  							<span id="footer_action_button" class='glyphicon'> </span>
  						</button>
  						<button type="button" class="btn btn-warning" data-dismiss="modal">
  							<span class='glyphicon glyphicon-remove'></span> Close
  						</button>
  					</div>
  				</div>
  			</div>
		  </div>
    </div>


    @include('includes.default.footer')
    @endsection
    <!-- Section Body Ends-->
    @section('script')
     <!-- Page-Level Scripts -->
     <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <script src="{{ asset('js/script.js') }}"></script>
    @endsection
