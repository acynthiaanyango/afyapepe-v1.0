@extends('layouts.facilityadmin')
@section('title', 'Dashboard')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
              <div class="col-lg-8">
                  <h2>Consultation Fees</h2>

              </div>
              <div class="col-lg-4">
                  <!-- <div class="title-action">
                      <a href="#" class="btn btn-white"><i class="fa fa-pencil"></i> Edit </a>
                      <a href="#" class="btn btn-white"><i class="fa fa-check "></i> Save </a>
                      <a href="invoice_print.html" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Print Invoice </a>
                  </div> -->
              </div>
    </div>
    <div class="wrapper wrapper-content">
      @if($fac_fee)
        <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right update23" id="update23">Update</span>
                                <h5>Consultation Fee</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">{{$fac_fee->new}} KES </h1>
                                <!-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div> -->
                                <small>New Patients Fee</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right update23" id="update23">Update</span>
                                <h5>Consultation Fee</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">{{$fac_fee->old}} KES</h1>
                                <!-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> -->
                                <small>Existing Patients Fee</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right update23" id="update23">Update</span>
                                <h5>Medical Report Fee</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">{{$fac_fee->medical_report_fee}} KES</h1>
                                <!-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> -->
                                <small> Fee</small>
                            </div>
                        </div>
                    </div>
                  </div>
@endif
  <div class="row">
  @if($fac_fee)
    <div class="col-lg-8 col-md-offset-2 ficha" id="updatediv"> @else <div class="col-lg-8" id="updatediv"> @endif
      <div class="ibox float-e-margins">
          <div class="ibox-title">
              <h5>Consultation Fee</h5>

          </div>
          <div class="ibox-content">
            <form class="form-horizontal" role="form" method="POST" action="/setfees" >
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if($fac_fee)<p>Update the required Facility Consultation Fee.</p> @else  <p>Set the required Facility Consultation Fee.</p>@endif

                  <div class="form-group"><label class="col-lg-2 control-label">New User's Fee</label>
                   <div class="col-lg-10">
                  <input type="text" value="@if($fac_fee){{$fac_fee->new}}@endif" class="form-control" name="new" required >
                  </div>
                  </div>

                  <div class="form-group"><label class="col-lg-2 control-label">Existing User's Fee</label>
                   <div class="col-lg-10">
                  <input type="text" value="@if($fac_fee){{$fac_fee->old}}@endif" class="form-control" name="old" required>
                  </div>
                  </div>

                  <div class="form-group"><label class="col-lg-2 control-label">Medical Report Fee</label>
                   <div class="col-lg-10">
                  <input type="text" value="@if($fac_fee){{$fac_fee->medical_report_fee}}@endif" class="form-control" name="med_report" required>
                  </div>
                  </div>

                  <div class="form-group">
                      <div class="col-lg-offset-2 col-lg-10">
                          <button class="btn btn-sm btn-primary" type="submit">@if($fac_fee)Update @else Submit @endif</button>
                      </div>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>
</div>


<script>
$('.update23').on('click', function(e){

    $("#updatediv").show();

});
</script>
@endsection
