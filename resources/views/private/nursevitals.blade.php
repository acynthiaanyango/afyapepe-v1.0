@extends('layouts.app2')
@section('title', 'Dashboard')

@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){
$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;
$Duser = $Docdata->user_id;
}

$db=DB::table('afya_users')->where('id',$id)->first();
$gender=$db->gender;
$dob=$db->dob;
$firstName = $db->firstname;
$secondName = $db->secondName;
$name =$firstName." ".$secondName;
$interval = date_diff(date_create(), date_create($dob));
$age= $interval->format(" %Y Years Old");

$facility=DB::table('facility_doctor')
->leftJoin('facilities', 'facility_doctor.facilitycode', '=', 'facilities.FacilityCode')
->where('user_id', Auth::id())->first();
$facilitydoc=$facility->doctor_id;
$facility_Name=$facility->FacilityName;


 ?>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-md-12">
     <br /><br />
      <div class="col-md-6">
        <address>
        <strong>PATIENT :</strong><br>
        Name: {{$name}}<br>
        Gender: {{$gender}}<br>
        Age: {{$age}}
      </address>
      </div>
      <div class="col-md-6 text-right">
        <address>
          <strong>DOCTOR :</strong><br>
          Name: {{$Name}}<br>
          Facility: {{$facility_Name}}<br>
      </address>
      </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Triage form <small>Add Patient Triage</small></h5>
                <div class="ibox-tools">

                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3 b-r"><h3 class="m-t-none m-b"></h3>
                        <!-- <p>Sign in today for more expirience.</p> -->
                        {!! Form::open(array('url' => 'private.createdetail','method'=>'POST')) !!}

                           <input type="hidden" class="form-control" id="exampleInputEmail1S" aria-describedby="emailHelp" value="{{$id}}" name="id"  required>
                           <div class="form-group">
                           <label for="exampleInputEmail1">Weight (kg)</label>
                           <input type="name" class="form-control"  placeholder="e.g ..80" name="weight"  >
                           </div>

                           <div class="form-group">
                           <label for="exampleInputEmail1">Height (cm)</label>
                           <input type="name" class="form-control" placeholder="e.g ..180" name="current_height" >
                           </div>
                          <div class="form-group">
                           <label for="exampleInputPassword1">Temperature (°C)</label>
                           <input type="name" class="form-control"  placeholder="e.g .. 37.2" name="temperature"  >
                          </div>

                           <div class="form-group">
                           <label for="exampleInputPassword1">Systolic BP</label>
                           <input type="name" class="form-control"  placeholder="Systolic BP" name="systolic">
                           </div>
</div>
  <div class="col-sm-3 b-r"><h3 class="m-t-none m-b"></h3>
                           <div class="form-group">
                           <label for="exampleInputEmail1">Diastolic BP</label>
                           <input type="name" class="form-control" aria-describedby="emailHelp" placeholder="Diastolic BP" name="diastolic" >
                           </div>

                           <div class="form-group">
                           <label for="exampleInputEmail1">RBS mmol/l</label>
                           <input type="name" class="form-control"  aria-describedby="emailHelp" placeholder="RBS mmol/l" name="rbs">
                           </div>

                           <div class="form-group">
                           <label for="exampleInputEmail1">HR b/min</label>
                           <input type="name" class="form-control"  aria-describedby="emailHelp" placeholder="HR b/min" name="hr">
                           </div>

                           <div class="form-group">
                           <label for="exampleInputEmail1">RR breaths/min</label>
                           <input type="name" class="form-control"  aria-describedby="emailHelp" placeholder="RR breaths/min" name="rr">
                           </div>

</div>
<div class="col-sm-6"><h4></h4>
                      <div class="form-group">
                         <label >Chief Complaint/Reason for visit:</label><br />
                         <select multiple="multiple" id="chief" name="chiefcompliant[]" class="form-control chief" style="width:50%" ></select>
                      </div>
                           <div class="form-group">
                           <label >Observation:</label><br />
                           <select multiple="multiple" id="observation" name="observation[]" class="form-control chief" style="width:50%"></select>
                       </div>
                          <div class="form-group">
                             <label >Symptom:</label><br />
                             <select multiple="multiple" id="symptom" name="symptoms[]" class="form-control chief" style="width:50%" ></select>
                         </div>

                          <input type="hidden" name="doctor" value="{{$facilitydoc}}">

                          <div class="form-group">
                          <label for="exampleInputPassword1">Notes</label><br />
                          <textarea class="form-control" name="nurse" >
                          </textarea>
                         </div>
                          @if($gender=='Female')

                          <div class="form-group">
                          <label for="exampleInputPassword1">Pregnant?</label>
                          <input type="radio" value="No"  name="pregnant"> No <input type="radio" value="Yes"  name="pregnant"> Yes
                          </div>
                          <div class="form-group" id="data_1">
                      <label for="exampleInputPassword1">LMP</label><br />
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                           <input type="text" class="form-control" name="lmp">
                                      </div>
                               </div>
                         @endif
                         <div class="pull-right">
                           <button type="submit" class="btn btn-primary">Submit</button>
                         </div>
                           {!! Form::close() !!}
    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  </div>














@endsection
