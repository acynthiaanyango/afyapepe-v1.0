@extends('layouts.private')
@section('title', 'Dashboard')
@section('content')
  <div class="content-page  equal-height">

      <div class="content">
          <div class="container">

            <div class="wrapper wrapper-content animated fadeInRight">
                      <div class="row">
                          <div class="col-lg-11">
                          <div class="ibox float-e-margins">
                              <div class="ibox-title">

                                  <div class="ibox-tools">
                                  </div>
                              </div>
                              <div class="ibox-content">

                              <div class="table-responsive">
                              <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Name</th>
                                      <th>Gender</th>
                                      <th>Age</th>
                                      <th>Date</th>
                                      <th>Diagnosis</th>
                                </tr>
                              </thead>

                              <tbody>
                                  <?php $i =1; ?>
                               @foreach($patients as $patient)
                                    <tr>
                                @if($patient->persontreated==="Self")
                            <td>{{$i}}</td>
                            <td>{{$patient->first}} {{$patient->second}}</td>
                            <td>{{$patient->gender}}</td>
                            <td>
                              <?php
                                  $dob=$patient->dob;
                                  $interval = date_diff(date_create(), date_create($dob));
                                  $age= $interval->format(" %Y Year, %M Months, %d Days Old");
                                ?>
                                {{$age}}
                             </td>
                              @else
                              <td>{{$i}}</td>
                              <td>{{$patient->dfirst}} {{$patient->dsecond}}</td>
                              <td>{{$patient->dgender}}</td>
                              <td>
                                <?php
                                  $ddob=$patient->ddob;
                                  $intervals = date_diff(date_create(), date_create($patient->ddob));
                                  $dage= $intervals->format(" %Y Year, %M Months, %d Days Old");
                                  ?>
                              {{$dage}}
                              </td>

                              @endif

                                <td>{{$patient->date_admitted}}</td>
                                <td>{{$patient->disease_name}}</td>

                                      </tr>
                                      <?php $i++; ?>

                                       @endforeach

                               </tbody>

                             </table>
                           </div>

                            </div>
                             </div>
                               </div>
                               </div>
                           </div>

                         </div>
@include('includes.default.footer')
          </div><!--content-->
      </div><!--content page-->

@endsection
