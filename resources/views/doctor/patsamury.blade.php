@extends('layouts.doctor_layout')
@section('title', 'Triage')
@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){
$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;
$set_up = $Docdata->set_up;
}
?>

<?php
foreach ($patientdetails as $pdetails) {

   $stat= $pdetails->status;
   $afyauserId= $pdetails->afya_user_id;
    $dependantId= $pdetails->persontreated;
    $app_id_prev= $pdetails->last_app_id;
    $app_id =  $pdetails->id;
    $doc_id= $pdetails->doc_id;
    $fac_id= $pdetails->facility_id;
    $fac_setup= $pdetails->set_up;
    $dependantAge = $pdetails->depdob;
    $AfyaUserAge = $pdetails->dob;
    $condition = $pdetails->condition;

    if($app_id_prev ==0){ $app_id2 = $app_id;
    }else{  $app_id2 = $app_id_prev;}

$now = time(); // or your date as well
$your_date = strtotime($dependantAge);
$datediff = $now - $your_date;
$dependantdays= floor($datediff / (60 * 60 * 24));


if ($dependantId =='Self') {
      $dob=$AfyaUserAge;
      $gender=$pdetails->gender;
    $firstName = $pdetails->firstname;
    $secondName = $pdetails->secondName;
    $name =$firstName." ".$secondName;
}

else {
     $dob=$dependantAge;
     $gender=$pdetails->depgender;
     $firstName = $pdetails->dep1name;
     $secondName = $pdetails->dep2name;
     $name =$firstName." ".$secondName;
}
$interval = date_diff(date_create(), date_create($dob));
$age= $interval->format(" %Y Year, %M Months, %d Days Old");

}
?>




<!--tabs Menus-->
@include('includes.doc_inc.topnavbar_v2')


<!--tabs Menus-->
  @include('includes.doc_inc.headmenu')

<div class="row wrapper border-bottom page-heading">
  <div class="ibox float-e-margins">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
      <div class="ibox-title">

      <h5>Summary</h5>

      </div>
      <div class="ibox-content">
      <div class="row">
      <div class="col-sm-12"><h3 class="m-t-none m-b"></h3>
      <form class="form-horizontal" role="form" method="POST" action="/summPatients" >
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
      {{ Form::hidden('afya_user_id',$afyauserId, array('class' => 'form-control')) }}


      <div class="form-group">
        <label class="col-lg-2 control-label">Family History </label><br>
          <div class="col-lg-10">
          <textarea class="form-control" rows="5"  name="fam_note">@if($fam){{$fam->notes}}@endif</textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-2 control-label">Patient Summary </label><br>
          <div class="col-lg-10">
          <textarea class="form-control" rows="5"  name="doc_note">@if($psummary){{$psummary->notes}}@endif</textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-2 control-label">Current Medication </label><br>
          <div class="col-lg-10">
          <textarea class="form-control" rows="3"  name="current_med">@if($cmed){{$cmed->drugs}}@endif</textarea>
          </div>
      </div>

      <div>
      <button class="btn btn-sm btn-primary pull-right" type="submit"><strong>@if($cmed || $psummary || $fam)UPDATE @else SUBMIT @endif </strong></button>
      </div>
      {{ Form::close() }}

      </div>
      </div>
      </div>

      </div>
    </div>





</div><!--tfloat-e-margins-->
</div><!--row wrapper-->
@endsection
@section('script')


@endsection
