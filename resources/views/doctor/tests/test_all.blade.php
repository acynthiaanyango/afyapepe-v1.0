@extends('layouts.doctor_layout')
@section('title', 'Test')
@section('content')
<?php
use App\Http\Controllers\Controller;
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){


$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;


}


      foreach ($patientD as $pdetails) {

         $stat= $pdetails->status;
         $afyauserId= $pdetails->afya_user_id;
          $dependantId= $pdetails->persontreated;
          $app_id_prev= $pdetails->last_app_id;
          $app_id =  $pdetails->id;
          $doc_id= $pdetails->doc_id;
          $fac_id= $pdetails->facility_id;
          $fac_setup= $pdetails->set_up;
          $dependantAge = $pdetails->depdob;
          $AfyaUserAge = $pdetails->dob;
          $condition = $pdetails->condition;

if($app_id_prev !==0){ $app_id2 = $app_id_prev;}else{$app_id2 = $app_id;}
 $now = time(); // or your date as well
 $your_date = strtotime($dependantAge);
 $datediff = $now - $your_date;
 $dependantdays= floor($datediff / (60 * 60 * 24));


 if ($dependantId =='Self') {
          $dob=$AfyaUserAge;
          $gender=$pdetails->gender;
          $firstName = $pdetails->firstname;
          $secondName = $pdetails->secondName;
          $name =$firstName." ".$secondName;
          $lmp = $pdetails->almp;
          $pregnant = $pdetails->apregnant;
   }

 else {    $dob=$dependantAge;
           $gender=$pdetails->depgender;
           $firstName = $pdetails->dep1name;
           $secondName = $pdetails->dep2name;
           $name =$firstName." ".$secondName;
           $lmp = $pdetails->dlmp;
           $pregnant = $pdetails->dpregnant;
      }


  $interval = date_diff(date_create(), date_create($dob));
  $age= $interval->format(" %Y Year, %M Months, %d Days Old");



}
?>
@include('includes.doc_inc.topnavbar_v2')

      <!-- @include('doctor.path') -->

<!--tabs Menus-->
  @include('includes.doc_inc.headmenu')

          <div class="row wrapper border-bottom">
             <div class="float-e-margins">
               <div class="col-lg-12">


  <?php $i =1; ?>
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>ALL TEST RESULTS</h5>
          <div class="ibox-tools">
            <a class="btn btn-primary"  href="{{route('alltestes',$app_id)}}"><i class="fa fa-angle-double-left"></i>&nbsp;BACK</a>
          </div>
        </div>

        <div class="ibox-content">
         <table class="table table-striped table-bordered table-hover dataTables-tests" >
           <thead>
        <tr>
        <th>No</th>
        <th>Date </th>
        <th>Test Name</th>
        <th>Test Type</th>
        <th>Action</th>

      </tr>
        </thead>

        <tbody>
          @foreach($otherimaging as $other)
                 <tr>
                 <td>{{ +$i }}</td>
                <td>{{$other->created_at}}</td>
                 <td>{{$other->tname}}</td>
                <td>{{Controller::test_type_bycat($other->test_cat_id)}}</td>
            @if($other->confirm =='N')
                  @if($other->done =='0')
                  <td>
                    {{ Form::open(['method' => 'DELETE','route' => ['imaging.deletes', $other->id],'style'=>'display:inline']) }}
                     {{ Form::submit('Remove', ['class' => 'btn btn-danger']) }}
                     {{ Form::close() }}
                 </td>

                  @else
                  <td>

                     {{ Form::open(array('route' => array('ctreport'),'method'=>'POST')) }}
                       {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
                       {{ Form::hidden('rtd_id',$other->id, array('class' => 'form-control')) }}
                       <button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Confirm Diagnosis</strong></button>
                      {{ Form::close() }}
                  </td>
                   @endif
             @else
             <td> Confirmed</td>
             @endif
           </tr>
               <?php $i++; ?>
           @endforeach









        @foreach($tstdone as $tstdn)
    <?php    $ptdid =$tstdn->ptdid;
      $prescs=$tstdn->done;
      if (is_null($prescs)) {
        $prescs= 'N/A';
      }
      elseif ($prescs==0) {
        $prescs= 'Pending';
      } elseif($prescs==1) {
        $prescs= 'Complete';
      }
        ?>
          <tr>
          <td>{{$i}}</td>
         <td>{{$tstdn->created_at}}</td>
         <td>{{$tstdn->name}}</td>
         <td>{{Controller::test_type($tstdn->tests_reccommended)}}</td>

           @if($tstdn->confirm =='N')
           @if($tstdn->done =='1')
          <td>
             {{ Form::open(array('route' => array('diaconf'),'method'=>'POST')) }}
               {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
               {{ Form::hidden('pat_details_id',$ptdid, array('class' => 'form-control')) }}
               <button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Confirm Diagnosis</strong></button>
              {{ Form::close() }}
          </td>
           @else
           <td>
             {{ Form::open(['method' => 'DELETE','route' => ['test.deletes', $tstdn->ptdid],'style'=>'display:inline']) }}
              {{ Form::submit('Remove', ['class' => 'btn btn-danger']) }}
              {{ Form::close() }}
          </td>
            @endif
      @else
      <td> Confirmed</td>
      @endif
    </tr>
        <?php $i++; ?>
    @endforeach

     @foreach($mri as $tstdn)

            <tr>
            <td>{{ +$i }}</td>
           <td>{{$tstdn->created_at}}</td>
            <td>{{$tstdn->tname}}</td>
           <td>{{Controller::test_type_bycat($tstdn->test_cat_id)}}</td>
     @if($tstdn->confirm =='N')
             @if($tstdn->done =='0')
             <td>
               {{ Form::open(['method' => 'DELETE','route' => ['imaging.deletes', $tstdn->id],'style'=>'display:inline']) }}
                {{ Form::submit('Remove', ['class' => 'btn btn-danger']) }}
                {{ Form::close() }}
            </td>

             @else
             <td>

                {{ Form::open(array('route' => array('ctreport'),'method'=>'POST')) }}
                  {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
                  {{ Form::hidden('rtd_id',$tstdn->id, array('class' => 'form-control')) }}
                  <button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Confirm Diagnosis</strong></button>
                 {{ Form::close() }}
             </td>
              @endif
        @else
        <td> Confirmed</td>
        @endif
      </tr>
          <?php $i++; ?>
      @endforeach

      @foreach($ct_scan as $tstdn)

            <tr>
            <td>{{ +$i }}</td>
           <td>{{$tstdn->created_at}}</td>
            <td>{{$tstdn->tname}}</td>
           <td>{{Controller::test_type_bycat($tstdn->test_cat_id)}}</td>
           @if($tstdn->confirm =='N')
             @if($tstdn->done =='0')
             <td>
               {{ Form::open(['method' => 'DELETE','route' => ['imaging.deletes', $tstdn->id],'style'=>'display:inline']) }}
                {{ Form::submit('Remove', ['class' => 'btn btn-danger']) }}
                {{ Form::close() }}
            </td>

             @else
             <td>
                {{ Form::open(array('route' => array('ctreport'),'method'=>'POST')) }}
                  {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
                  {{ Form::hidden('rtd_id',$tstdn->id, array('class' => 'form-control')) }}
                  <button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Confirm Diagnosis</strong></button>
                 {{ Form::close() }}
             </td>
              @endif
        @else
        <td> Confirmed</td>
        @endif
      </tr>
          <?php $i++; ?>
      @endforeach

       @foreach($ultrasound as $tstdn)

          <tr>
          <td>{{ +$i }}</td>
         <td>{{$tstdn->created_at}}</td>
         <td>{{$tstdn->tname}}</td>
         <td>{{Controller::test_type_bycat($tstdn->test_cat_id)}}</td>
   @if($tstdn->confirm =='N')
           @if($tstdn->done =='0')
           <td>
             {{ Form::open(['method' => 'DELETE','route' => ['imaging.deletes', $tstdn->id],'style'=>'display:inline']) }}
              {{ Form::submit('Remove', ['class' => 'btn btn-danger']) }}
              {{ Form::close() }}
          </td>

           @else
           <td>

              {{ Form::open(array('route' => array('ultrareport'),'method'=>'POST')) }}
                {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
                {{ Form::hidden('rtd_id',$tstdn->id, array('class' => 'form-control')) }}
                <button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Confirm Diagnosis</strong></button>
               {{ Form::close() }}
           </td>
            @endif
      @else
      <td> Confirmed</td>
      @endif
    </tr>
        <?php $i++; ?>
    @endforeach

    @foreach($xray as $tstdn)

                              <tr>
                              <td>{{ +$i }}</td>
                             <td>{{$tstdn->created_at}}</td>
                             <td>{{$tstdn->tname}}</td>
                             <td>{{Controller::test_type_bycat($tstdn->test_cat_id)}}</td>
                         @if($tstdn->confirm =='N')
                               @if($tstdn->done =='0')
                               <td>
                                 {{ Form::open(['method' => 'DELETE','route' => ['imaging.deletes', $tstdn->id],'style'=>'display:inline']) }}
                                  {{ Form::submit('Remove', ['class' => 'btn btn-danger']) }}
                                  {{ Form::close() }}
                              </td>

                               @else
                               <td>

                                  {{ Form::open(array('route' => array('xrayreport'),'method'=>'POST')) }}
                                    {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
                                    {{ Form::hidden('rtd_id',$tstdn->id, array('class' => 'form-control')) }}
                                    <button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Confirm Diagnosis</strong></button>
                                   {{ Form::close() }}
                               </td>
                                @endif
                          @else
                          <td> Confirmed</td>
                          @endif
                        </tr>
                            <?php $i++; ?>
                        @endforeach
        </tbody>
      </table>

  </div>
</div>

      </div><!-- col md 12" -->
   </div><!-- emargis" -->
   </div>
@endsection
@section('script-test')
 <!-- Page-Level Scripts -->
<script src="{{ asset('js/tests.js') }}"></script>
@endsection
