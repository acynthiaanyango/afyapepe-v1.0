@extends('layouts.doctor_layout')
@section('title', 'Test')
@section('styles')
<link rel="stylesheet" href="{{asset('css/custombuttons.css') }}" />
@endsection
@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){


$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;


}


      foreach ($patientD as $pdetails) {

         $stat= $pdetails->status;
         $afyauserId= $pdetails->afya_user_id;
          $dependantId= $pdetails->persontreated;
          $app_id_prev= $pdetails->last_app_id;
          $app_id =  $pdetails->id;
          $doc_id= $pdetails->doc_id;
          $fac_id= $pdetails->facility_id;
          $fac_setup= $pdetails->set_up;
          $dependantAge = $pdetails->depdob;
          $AfyaUserAge = $pdetails->dob;
          $condition = $pdetails->condition;

if($app_id_prev !==0){ $app_id2 = $app_id_prev;}else{$app_id2 = $app_id;}
 $now = time(); // or your date as well
 $your_date = strtotime($dependantAge);
 $datediff = $now - $your_date;
 $dependantdays= floor($datediff / (60 * 60 * 24));


 if ($dependantId =='Self') {
          $dob=$AfyaUserAge;
          $gender=$pdetails->gender;
          $firstName = $pdetails->firstname;
          $secondName = $pdetails->secondName;
          $name =$firstName." ".$secondName;
          $lmp = $pdetails->almp;
          $pregnant = $pdetails->apregnant;
   }

 else {    $dob=$dependantAge;
           $gender=$pdetails->depgender;
           $firstName = $pdetails->dep1name;
           $secondName = $pdetails->dep2name;
           $name =$firstName." ".$secondName;
           $lmp = $pdetails->dlmp;
           $pregnant = $pdetails->dpregnant;
      }


  $interval = date_diff(date_create(), date_create($dob));
  $age= $interval->format(" %Y Year, %M Months, %d Days Old");


 $appStatue=$stat;
if ($appStatue == 2) {
  $appStatue ='ACTIVE';
} elseif ($stat == 3) {
  $appStatue='Discharged Outpatient';
} elseif ($stat == 4) {
  $appStatue='Admitted';
} elseif ($stat == 5) {
  $appStatue='Refered';
}
elseif ($stat == 6) {
  $appStatue='Discharged Intpatient';
}
elseif ($stat == 7) {
  $appStatue='Waiting Test Result';
}
}
?>

@include('includes.doc_inc.topnavbar_v2')

<!--tabs Menus-->
  @include('includes.doc_inc.headmenu')

        <!--tabs Menus-->
<div class="row wrapper border-bottom">
    <div class="float-e-margins">
          <div class="col-md-10">


<div class="well text-center col-md-12">
  <p class="text-left"> All Tests results </p>
  <a href="{{url('test-all',$app_id)}}" class="btn btn-w-m btn-primary">TEST RESULTS</a>

</div>
<p class="text-center">
  Select tests to recommend
</p>
      <div class="well text-center col-md-6">
        <a href="{{route('testes',$app_id)}}" class="btn btn-w-m btn-primary">LAB TESTS</a>
        <!-- <a href="#" class="button red alt">Push</a> -->
      </div>

      <div class="well text-center col-md-6">
        <a href="{{route('testesmri',$app_id)}}" class="btn btn-w-m btn-primary">MRI TESTS</a>
          <a href="{{route('otherimaging',$app_id)}}" class="btn btn-w-m btn-primary">OTHER IMAGING TESTS</a>
        <!-- <a href="#" class="button blue alt">Push</a> -->
      </div>

      <div class="well text-center col-md-6">
        <a href="{{route('testesImage',$app_id)}}" class="btn btn-w-m btn-primary">CT-SCAN TESTS</a>
        <!-- <a href="#" class="button green alt">Push</a> -->
      </div>


  <div class="well text-center col-md-6">
        <a href="{{route('testesultra',$app_id)}}" class="btn btn-w-m btn-primary">ULTRASOUND TESTS</a>
        <!-- <a href="#" class="button brown alt">Push</a> -->
      </div>

      <div class="well text-center col-md-6">
        <a href="{{route('testesxray',$app_id)}}" class="btn btn-w-m btn-primary">XRAY TESTS</a>
        <!-- <a href="#" class="button pink alt">Push</a> -->
      </div>

      <div class="well text-center col-md-6">
        <a href="#" class="btn btn-w-m btn-primary">Neurology </a>
        <a href="#" class="btn btn-w-m btn-primary">Gastrointestinal</a>
      </div>



      </div><!-- col md 12" -->
   </div><!-- emargis" -->
   </div>
@endsection
@section('script-test')
 <!-- Page-Level Scripts -->
<script src="{{ asset('js/tests.js') }}"></script>
@endsection
