@extends('layouts.doctor_layout')
@section('title', 'End Visit')
@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){
$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;
$Duser = $Docdata->user_id;
}


      foreach ($patientD as $pdetails) {
        // $patientid = $pdetails->pat_id;
        //  $facilty = $pdetails->FacilityName;
         $stat= $pdetails->status;
         $afyauserId= $pdetails->afya_user_id;
          $dependantId= $pdetails->persontreated;
          $app_id= $pdetails->id;
          $doc_id= $pdetails->doc_id;
          $fac_id= $pdetails->facility_id;
          $fac_setup= $pdetails->set_up;
          $dependantAge = $pdetails->depdob;
          $AfyaUserAge = $pdetails->dob;
          $condition = $pdetails->condition;

 $now = time(); // or your date as well
 $your_date = strtotime($dependantAge);
 $datediff = $now - $your_date;
 $dependantdays= floor($datediff / (60 * 60 * 24));


 if ($dependantId =='Self') {
            $dob=$AfyaUserAge;
            $gender=$pdetails->gender;
          $firstName = $pdetails->firstname;
          $secondName = $pdetails->secondName;
          $name =$firstName." ".$secondName;
   }

 else {    $dob=$dependantAge;
           $gender=$pdetails->depgender;
           $firstName = $pdetails->dep1name;
           $secondName = $pdetails->dep2name;
           $name =$firstName." ".$secondName;
      }


  $interval = date_diff(date_create(), date_create($dob));
  $age= $interval->format(" %Y Year, %M Months, %d Days Old");


 $appStatue=$stat;
if ($appStatue == 2) {
  $appStatue ='ACTIVE';
} elseif ($stat == 3) {
  $appStatue='Discharged Outpatient';
} elseif ($stat == 4) {
  $appStatue='Admitted';
} elseif ($stat == 5) {
  $appStatue='Refered';
}
elseif ($stat == 6) {
  $appStatue='Discharged Intpatient';
}

}
?>
@include('includes.doc_inc.topnavbar_v2')


<!--tabs Menus-->
  @include('includes.doc_inc.headmenu')
<!--tabs Menus-->

<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">

      <div class="ibox float-e-margins">
        <div class="ibox-content">
 <div class="form-group"><label class="col-md-2 control-label">Next Visit Date</label>
    <div class="col-md-10">
     <label class="checkbox-inline"><input type="radio" name="visitend" id="visity">YES</label>
     <label class="checkbox-inline"><input type="radio" name="visitend" id="visitn">NO</label>
  </div>
 </div>
</div>
</div>
<div id="visityes" class="ficha">
      <div class="ibox float-e-margins">
  {{ Form::open(array('route' => array('nxtappt'),'method'=>'POST')) }}

<div class="form-group col-md-8 col-md-offset-1" id="data_1">
                <label class="font-normal">Next Doctor Visit</label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" class="form-control" name="next_appointment" value="">
                </div>
            </div>
            <div class="form-group col-md-8 col-md-offset-1" id="data_1">
                <label class="font-normal">Time For Visit</label>
                <div class="input-group clockpicker" data-autoclose="true">
              <input type="text" class="form-control" name="next_time" placeholder="09:30" > <span class="input-group-addon">
                      <span class="fa fa-clock-o"></span>   </span>
                    </div>
            </div>
            <div class="form-group col-md-8 col-md-offset-1">
            <label for="role" class="control-label">Doctor note</label>
            {{ Form::textarea('doc_note', null, array('placeholder' => 'note..','class' => 'form-control col-lg-8')) }}
            </div>

      {{ Form::hidden('appointment_status',10, array('class' => 'form-control')) }}
      {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
      {{ Form::hidden('doc_id',$doc_id, array('class' => 'form-control')) }}
<div class="form-group  col-md-8 col-md-offset-1">
  <button type="SUBMIT" class="btn btn-primary">Submit</button>
</div>
{{ Form::close() }}
 </div>
</div>

<div id="visitnop" class="ficha">
  {{ Form::open(array('route' => array('nxtappt'),'method'=>'POST')) }}
  {{ Form::hidden('appointment_status',10, array('class' => 'form-control')) }}
  {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
  {{ Form::hidden('doc_id',$doc_id, array('class' => 'form-control')) }}
  <div class="form-group  col-md-8 col-md-offset-1">
    <button type="submit" class="btn btn-primary">DONE</button>
  </div>
  {{ Form::close() }}
</div>


          </div><!--tabs-container-->
        </div><!--col12-->
        <!--tabs-->

@endsection
@section('script-test')
<!-- Put your page  scripts here -->

<script>
$(document).ready(function(){
    $('#visity').change(function(){
        if(this.checked)
            $('#visityes').fadeIn('slow');
            $('#visitnop').hide('fast');
    });
    $('#visitn').change(function(){
        if(this.checked)
            $('#visitnop').fadeIn('slow');
            $('#visityes').hide('fast');
    });
});
</script>
@endsection
