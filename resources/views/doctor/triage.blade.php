<?php $pTriagedetails = DB::table('appointments')
 ->leftjoin('triage_details','appointments.id','=','triage_details.appointment_id')
->select('triage_details.*')
 ->where('appointments.id',$app_id)
 ->get();

  $gexam = DB::table('general_examination')->where('appointment_id',$app_id)->first();
  $prem = DB::table('result_sofar')->where('appointment_id',$app_id)->first();
  $triage = DB::table('triage_details')->where('appointment_id',$app_id)->first();


  ?>
<div class="wrapper wrapper-content">

   <div class="col-lg-12">
                       <div class="ibox float-e-margins">
                           <div class="ibox-title">
                               <h5>Today's Vitals </h5>
                               <div class="ibox-tools">
                               </div>
                           </div>
                           <div class="ibox-content">
                   <div class="row">
                   {!! Form::open(array('route' => 'trgpost','method'=>'POST')) !!}
                     <div class="col-lg-3 b-r">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">

                         <input type="hidden" class="form-control"  value="@if($triage){{$triage->user_id}} @endif" name="id">
                         <input type="hidden" class="form-control" value="@if($triage){{$triage->appointment_id}} @endif" name="appointment_id"  >

                       <div class="form-group">
                       <label for="exampleInputEmail1">Weight (kg)</label>
                       <input type="text" class="form-control"  value="@if($triage){{$triage->current_weight}} @endif" name="weight"  >
                       </div>

                       <div class="form-group">
                       <label for="exampleInputEmail1">Height (cm)</label>
                       <input type="text" class="form-control" value="@if($triage){{$triage->current_height}} @endif" name="current_height">
                       </div>
                      <div class="form-group">
                       <label for="exampleInputPassword1">Temperature °C</label>
                       <input type="text" class="form-control" value="@if($triage){{$triage->temperature}} @endif" name="temperature"  >
                      </div>

                       <div class="form-group">
                       <label for="exampleInputPassword1">Systolic BP</label>
                       <input type="text" class="form-control"  value="@if($triage){{$triage->systolic_bp}} @endif" name="systolic" >
                   </div>
</div>
 <div class="col-lg-3 br">
                       <div class="form-group">
                       <label for="exampleInputEmail1">Diastolic BP</label>
                       <input type="text" class="form-control"   value="@if($triage){{$triage->diastolic_bp}} @endif" name="diastolic">
                       </div>

                       <div class="form-group">
                       <label for="exampleInputEmail1">RBS mmol/l</label>
                       <input type="name" class="form-control"   value="@if($triage){{$triage->rbs}} @endif" name="rbs">
                       </div>

                       <div class="form-group">
                       <label for="exampleInputEmail1">HR b/min</label>
                       <input type="name" class="form-control"  value="@if($triage){{$triage->hr}} @endif" name="hr">
                       </div>

                       <div class="form-group">
                       <label for="exampleInputEmail1">RR breaths/min</label>
                       <input type="name" class="form-control"   value="@if($triage){{$triage->rr}} @endif" name="rr">
                       </div>


                     </div>

                     <div class="col-lg-6 ">


           <?php if($triage){$comp = $triage->chief_compliant;}else{$comp = '';}
                 if($triage){$sympt = $triage->symptoms;}else{$sympt = '';}
                 if($triage){$observ = $triage->observation;}else{$observ = '';}
                         ?>

                         <div class="form-group">
                         <label >Chief Complaint:</label><br />
                         <select multiple="multiple" id="chief" name="chiefcomplaint[]" class="form-control chief" style="width:80%">

                           <option selected="selected">{{$comp}}</option>

                         </select>
                         </div>



                         <div class="form-group">
                         <label >Observation:</label><br />
                         <select multiple="multiple" id="observation" name="observation[]" class="form-control chief" style="width:80%">

                           <option selected="selected">{{$observ}}</option>

                         </select>
                         </div>



                         <div class="form-group">
                         <label >Symptom:</label><br />
                         <select multiple="multiple" id="symptom" name="symptoms[]" class="form-control chief" style="width:80%">

                           <option selected="selected">{{$sympt}}</option>

                         </select>
                         </div>
 </div>
  <div class="col-md-7">
                         <div class="form-group">
                         <label for="exampleInputPassword1">Nurse Notes</label><br />
                         <textarea class="form-control"  name="nurse" >@if($triage){{$triage->Doctor_note}} @endif</textarea>
                         </div>
 </div>
  <div class="col-md-2">
                          <?php
                          $db = DB::table('afya_users')
                              ->where('id',$afyauserId)
                              ->first();

                          $gender = $db->gender;
                           ?>
                          @if($gender == 'Female')
                         <div class="form-group">
                        <label for="exampleInputPassword1">Pregnant?</label><br />
                        <input type="radio" value="No"  name="pregnant" <?php echo ($triage->pregnant=='No')?'checked':'' ?>> No
                        <input type="radio" value="Yes"  name="pregnant" <?php echo ($triage->pregnant=='Yes')?'checked':'' ?>> Yes

                        <!-- <input type="text" class="form-control"   value="@if($triage){{$triage->pregnant}} @endif" name="pregnant"> -->
                          </div>
    </div>
     <div class="col-md-3">
                          <div class="form-group">
                          <label for="exampleInputEmail1">LMP </label>
                          <input type="text" class="form-control"   value="@if($triage){{$triage->lmp}} @endif" name="lmp">
                          </div>

                          @endif

                   <button type="submit" class="btn btn-primary">@if($triage)UPDATE @else SUBMIT @endif</button>
                     </div>
                     {!! Form::close() !!}
                   </div>
 </div>
</div>
</div>

                                   <!-- General Examinations -->
                                   <div class="wrapper wrapper-content">
                                   <div class="col-lg-12">
                                   <div class="ibox float-e-margins">
                                   <div class="ibox-title">
                                   <h5>Examinations</h5>

                                   </div>
                                   <div class="ibox-content">
                                   <div class="row">
                                   <div class="col-sm-6 b-r"><h3 class="m-t-none m-b"></h3>
                                   <form class="form-horizontal" role="form" method="POST" action="/generalExamination" novalidate>
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                   {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
                                   <div class="form-group"><label>General Examination</label> <input type="text" value="@if($gexam){{$gexam->g_examination}}@endif" name="g_examination" class="form-control"></div>
                                   <div class="form-group"><label>CVS</label> <input type="text" value="@if($gexam){{$gexam->cvs}}@endif" name="cvs" class="form-control"></div>
                                   <div class="form-group"><label>RS</label> <input type="text" value="@if($gexam){{$gexam->rs}}@endif" name="rs" class="form-control"></div>
                                   <div class="form-group"><label>PA</label> <input type="text" value="@if($gexam){{$gexam->pa}}@endif" name="pa" class="form-control"></div>



                                   </div>
                                   <div class="col-sm-6"><h4></h4>
                                     <div class="form-group"><label>CNS</label> <input type="text" value="@if($gexam){{$gexam->cns}}@endif" name="cns" class="form-control"></div>
                                     <div class="form-group"><label>MSS</label> <input type="text" value="@if($gexam){{$gexam->mss}}@endif" name="mss" class="form-control"></div>
                                     <div class="form-group"><label>PERIPHERIES</label> <input type="text" value="@if($gexam){{$gexam->peripheries}}@endif" name="peripheries" class="form-control"></div>
                                  </div>

                                  <div class="form-group">
                                  <div class="col-lg-8"><label>Result So Far </label></div>
                                  <div class="col-lg-10">
                                  <textarea class="form-control" rows="5"  name="results">@if($prem){{$prem->notes}}@endif</textarea>
                                  </div>
                                </div>




                                   <div class="col-lg-4 col-md-offset-8">
                                   <button class="btn btn-sm btn-primary pull-right" type="submit"><strong>@if($gexam || $prem)UPDATE @else SUBMIT @endif</strong></button>
                                   </div>
                                   {{ Form::close() }}


                                   </div>
                                   </div>
                                   </div>
                                   </div>
                                   </div>

</div>
