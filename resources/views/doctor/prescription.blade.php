@extends('layouts.doctor_layout')
@section('title', 'Prescriptions')

@section('content')



<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){


$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;


}


      foreach ($patientD as $pdetails) {
        // $patientid = $pdetails->pat_id;
        //  $facilty = $pdetails->FacilityName;
         $stat= $pdetails->status;
         $afyauserId= $pdetails->afya_user_id;
          $dependantId= $pdetails->persontreated;
          $app_id= $pdetails->id;
          $doc_id= $pdetails->doc_id;
          $fac_id= $pdetails->facility_id;
          $fac_setup= $pdetails->set_up;
          $dependantAge = $pdetails->depdob;
          $AfyaUserAge = $pdetails->dob;
          $condition = $pdetails->condition;

 $now = time(); // or your date as well
 $your_date = strtotime($dependantAge);
 $datediff = $now - $your_date;
 $dependantdays= floor($datediff / (60 * 60 * 24));


 if ($dependantId =='Self') {
            $dob=$AfyaUserAge;
            $gender=$pdetails->gender;
          $firstName = $pdetails->firstname;
          $secondName = $pdetails->secondName;
          $name =$firstName." ".$secondName;
   }

 else {    $dob=$dependantAge;
           $gender=$pdetails->depgender;
           $firstName = $pdetails->dep1name;
           $secondName = $pdetails->dep2name;
           $name =$firstName." ".$secondName;
      }


  $interval = date_diff(date_create(), date_create($dob));
  $age= $interval->format(" %Y Year, %M Months, %d Days Old");



}
?>

          @include('includes.doc_inc.topnavbar_v2')



          <!--tabs Menus-->
            @include('includes.doc_inc.headmenu')
  <!--tabs Menus-->

          <?php  $routem= (new \App\Http\Controllers\TestController);
                $routems = $routem->RouteM();

            $Strength= (new \App\Http\Controllers\TestController);
                $Strengths = $Strength->Strength();

             $frequency= (new \App\Http\Controllers\TestController);
                $frequent = $frequency->Frequency();

          use App\Http\Controllers\Controller;

          $diz =DB::table('patient_diagnosis')
                  ->select('disease_id as name','id')
                  ->where('patient_diagnosis.appointment_id',$app_id)
                  ->get();

          ?>


          <div class="wrapper wrapper-content animated">


                  <div class="row">
                      <div class="col-lg-12">
                          <div class="tabs-container">
                              <ul class="nav nav-tabs">
                                  <li class="active"><a data-toggle="tab" href="#tab-51">Add Prescription</a></li>
                                  <li class=""><a data-toggle="tab" href="#tab-52">Prescription History</a></li>
                              </ul>
                              <div class="tab-content">
                                  <div id="tab-51" class="tab-pane active">
                                      <div class="panel-body">

                                  <div class="">
                                        <h5>Today Prescriptions</h5>
                                        <div class="ibox-tools">
                                     <button class="add-modal btn btn-primary"  data-appid="{{$app_id}}" data-facility="{{$fac_id}}">Add Medicines</button>
                                     <a class="btn btn-primary"  href="{{route('printpresc',$app_id)}}" >Print Prescription</a>

                                      </div>
                                        </div>
                                        <div class="ibox-content">

                                        <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-tests" >
                                        <thead>
                                        <tr>
                                          <th>#</th>
                                            <th>Condition</th>
                                          <th>Drug Name</th>
                                          <th>Route</th>
                                          <th>Frequency</th>
                                          <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i=1;?>
                                        @foreach ($presczs as $ctt)

                                          <tr class="item{{$ctt->presc_details_id}}">
                                            <td>{{$i}}</td>
                                            <td>{{$ctt->name}}</td>
                                            <td>{{$ctt->drug_id}}</td>
                                            <td>{{$ctt->abbreviation}}  {{$ctt->rotename}}</td>
                                            <td>{{$ctt->frequencyname}}</td>
                                          <td><a class="btn btn-danger" href="{{route('prescs.deletes',$ctt->presc_details_id)}}"><i class="fa fa-remove"></i>Delete</a></td>


                                        </tr>
                                        <?php $i++;  ?>
                                        @endforeach
                                        </tbody>

                                        </tbody>
                                        <tfoot>
                                        <tr>

                                        </tr>
                                        </tfoot>
                                        </table>
                                        </div>

                                        </div>








                                      </div>
                                  </div>
                                  <div id="tab-52" class="tab-pane">
                                      <div class="panel-body">
                                        <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-tests" >
                                        <thead>
                                        <tr>
                                          <th>#</th>
                                            <th>Condition</th>
                                          <th>Drug Name</th>
                                          <th>Route</th>
                                          <th>Frequency</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i=1;?>
                                        @foreach ($allpresczs as $ctt)

                                          <tr class="item{{$ctt->presc_details_id}}">
                                            <td>{{$i}}</td>
                                            <td>{{$ctt->name}}</td>
                                            <td>{{$ctt->drug_id}}</td>
                                            <td>{{$ctt->abbreviation}}  {{$ctt->rotename}}</td>
                                            <td>{{$ctt->frequencyname}}</td>


                                        </tr>
                                        <?php $i++;  ?>
                                        @endforeach
                                        </tbody>

                                        </tbody>
                                        <tfoot>
                                        <tr>

                                        </tr>
                                        </tfoot>
                                        </table>
                                        </div>




                                      </div>
                                  </div>
                              </div>


                          </div>
                      </div>
                    </div>
                  </div>





            <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              {{ Form::open(array('url' => array('insert-presc-detail'),'method'=>'POST', 'class'=>'form-horizontal')) }}


              <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <div class="form-group adds">
              <label for="tag_list" class="">Diagnosed Disease:</label>
              <select class="form-control"  name="disease"  id="disease" style="width:100;">
                    @foreach($diz as $dizs)
                    <option value=" ">Select</option>
                      <option value="{{$dizs->id }}">{{ $dizs->name  }} </option>
                   @endforeach
                </select>
              </div>

            <div class="form-group">
            <label>Drug Name:</label>
            <input type="text" class="form-control" id="drug" name="drug" required />
            <input type="hidden" class="form-control" id="appid" name="appointment" >
            <input type="hidden" class="form-control" id="facility" name="facility" >
            <input type="hidden" class="form-control" value="Normal" name="state" >

            </div>

            <div class="form-group adds">
            <label for="tag_list" class="">Route:</label>
            <select class="form-control" name="routes" id="route" style="width:100;">
                  @foreach($routems as $routemz)
                    <option value="{{$routemz->id }}">{{ $routemz->abbreviation }}----{{ $routemz->name  }} </option>
                 @endforeach
              </select>
            </div>

            <div class="form-group adds">
            <label for="tag_list" class="">Frequency:</label>
            <select class="form-control"  name="frequent"  id="frequency" style="width:100;">
                  @foreach($frequent as $freq)
                    <option value="{{$freq->id }}">{{ $freq->abbreviation }}----{{ $freq->name  }} </option>
                 @endforeach
              </select>
            </div>

            <div class="form-group adds">
            <label for="tag_list" class="">Strength:</label>
            <select class="form-control"  name="strength"  id="strength" style="width:100;">
                  @foreach($Strengths as $trth)
                    <option value="{{$trth->id }}">{{ $trth->strength }}</option>
                 @endforeach
              </select>
            </div>

            <div class="form-group adds">
            <label for="tag_list" class="">Units:</label>
            <select class="form-control" name="unit" style="width: 100%">
            <option value=''>Choose one</option>
            <option value='ml'>ml</option>
            <option value='mg'>Mg</option>

            </select>
            </div>



            </div>

            <div class="modal-footer">
              <!-- <button type="button" class="btn actionBtn" data-dismiss="modal">
                <span id="footer_action_button" class='glyphicon'> </span>
              </button> -->

              <button type="submit" class="btn btn-primary actionBtn">Submit</button>


              <button type="button" class="btn btn-warning" data-dismiss="modal">
            <span class='glyphicon glyphicon-remove'></span> Close
            </button>
            {!! Form::close() !!}
            </div>
            </div>
            </div>
            </div>
@endsection
<!-- Section Body Ends-->
@section('script-test')
<!-- Put your scripts here -->
<script src="{{ asset('js/prescription.js') }}"></script>
@endsection
