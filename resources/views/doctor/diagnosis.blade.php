@extends('layouts.doctor_layout')
@section('title', 'Quick Diagnosis')
@section('content')


<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){


$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;
}


      foreach ($patientD as $pdetails) {
        // $patientid = $pdetails->pat_id;
        //  $facilty = $pdetails->FacilityName;
         $stat= $pdetails->status;
         $afyauserId= $pdetails->afya_user_id;
          $dependantId= $pdetails->persontreated;
          $app_id_prev= $pdetails->last_app_id;
          $app_id= $pdetails->id;
          $doc_id= $pdetails->doc_id;
          $fac_id= $pdetails->facility_id;
          $fac_setup= $pdetails->set_up;
          $dependantAge = $pdetails->depdob;
          $AfyaUserAge = $pdetails->dob;
        $condition = $pdetails->condition;
 $now = time(); // or your date as well
 $your_date = strtotime($dependantAge);
 $datediff = $now - $your_date;
 $dependantdays= floor($datediff / (60 * 60 * 24));
 if($app_id_prev){ $app_id2 = $app_id_prev;}else{$app_id2 = $app_id;}


 if ($dependantId =='Self') {
            $dob=$AfyaUserAge;
            $gender=$pdetails->gender;
          $firstName = $pdetails->firstname;
          $secondName = $pdetails->secondName;
          $name =$firstName." ".$secondName;
   }

 else {    $dob=$dependantAge;
           $gender=$pdetails->depgender;
           $firstName = $pdetails->dep1name;
           $secondName = $pdetails->dep2name;
           $name =$firstName." ".$secondName;
      }


  $interval = date_diff(date_create(), date_create($dob));
  $age= $interval->format(" %Y Year, %M Months, %d Days Old");


 $appStatue=$stat;
if ($appStatue == 2) {
  $appStatue ='ACTIVE';
} elseif ($stat == 3) {
  $appStatue='Discharged Outpatient';
} elseif ($stat == 4) {
  $appStatue='Admitted';
} elseif ($stat == 5) {
  $appStatue='Refered';
}
elseif ($stat == 6) {
  $appStatue='Discharged Intpatient';
}

}
?>


@include('includes.doc_inc.topnavbar_v2')

      <!-- @include('doctor.path') -->

<!--tabs Menus-->
  @include('includes.doc_inc.headmenu')
<!--tabs Menus-->

<div class="row wrapper border-bottom  page-heading">
  <div class="col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
          Patient Diagnosis
        </div>
        <div class="panel-body">
          <?php $condy=DB::table('patient_diagnosis')
          ->Where('appointment_id',$app_id)
         ->select('patient_diagnosis.id','patient_diagnosis.disease_id as name','patient_diagnosis.date_diagnosed')
         ->get();
          ?>
          <ul class="list-group">
      <li class="list-group-item">
      <span class="badge badge-info">Date Diagnosed</span><strong>Disease</strong></li>

          @foreach($condy as $diags)
    <li class="list-group-item"><span class="badge">{{$diags->date_diagnosed}}</span>{{$diags->name}}</li>

          @endforeach
          </ul>
        </div>
    </div>
</div>
  <div class="col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
          Patient Diagnosis
        </div>
        <div class="panel-body">

{{ Form::open(array('route' => array('quickdiag'),'method'=>'POST' ,'class'=>'form-horizontal')) }}

<div class="form-group ">
<label  class="col-lg-4 control-label">Condition:</label>
<div class="col-lg-6">
  <input type="text" class="form-control" value="" name="disease"  >

  <!-- <select id="diseases" name="disease" class="form-control d_list2" style="width: 100%"></select> -->
</div>
</div>

<div class="form-group">
<label class="col-lg-4 control-label">Type of Diagnosis:</label>
<div class="col-lg-6"><select class="form-control" name="level"  style="width: 100%" >
<option value=''>Choose one</option>
<option value='Primary'>Primary</option>
<option value='Secondary'>Secondary</option>
</select>
</div></div>

<div class="form-group">
<label class="col-lg-4 control-label">Chronic:</label>
<div class="col-lg-6"><select class="form-control" name="chronic"  style="width: 100%" >
<option value=''>Choose one</option>
<option value='Y'>YES</option>
<option value='N'>No</option>
</select>
</div></div>
<div class="form-group">
<label class="col-lg-4 control-label">Level of Severity:</label>
<div class="col-lg-6"><select class="form-control" name="severity"  style="width: 100%" >
<?php $severeity=DB::table('severity')->get();
?>
<option value=''>Choose one</option>
@foreach($severeity as $diag)
<option value='{{$diag->id}}'>{{$diag->name}}</option>
@endforeach
</select>
</div></div>

<div class="form-group">
<label class="col-lg-4 control-label">Supportive Care:</label>
<div class="col-lg-6"><select class="form-control" name="care"  style="width: 100%" >
<?php $scare=DB::table('supportive_care')->get();
?>
<option value=''>Choose one</option>
@foreach($scare as $sup)
<option value='{{$sup->name}}'>{{$sup->name}}</option>
@endforeach
</select>
</div>
</div>
{{ Form::hidden('state','Normal', array('class' => 'form-control')) }}
{{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
{{ Form::hidden('afya_user_id',$afyauserId, array('class' => 'form-control')) }}

<div class="col-lg-offset-5">
<button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Submit</strong></button>
</div>
{{ Form::close() }}

 </div><!-- tab content -->
  </div><!-- col md 12" -->
   </div><!-- "ibox float-e-margins" -->
</div><!-- row -->





@endsection
