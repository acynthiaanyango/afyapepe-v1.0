@extends('layouts.doctor_layout')
@section('title', 'Triage')
@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){
$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;
$set_up = $Docdata->set_up;
}
?>

<?php
foreach ($patientdetails as $pdetails) {

   $stat= $pdetails->status;
   $afyauserId= $pdetails->afya_user_id;
    $dependantId= $pdetails->persontreated;
    $app_id_prev= $pdetails->last_app_id;
    $app_id =  $pdetails->id;
    $doc_id= $pdetails->doc_id;
    $fac_id= $pdetails->facility_id;
    $fac_setup= $pdetails->set_up;
    $dependantAge = $pdetails->depdob;
    $AfyaUserAge = $pdetails->dob;
    $condition = $pdetails->condition;

    if($app_id_prev ==0){ $app_id2 = $app_id;
    }else{  $app_id2 = $app_id_prev;}

$now = time(); // or your date as well
$your_date = strtotime($dependantAge);
$datediff = $now - $your_date;
$dependantdays= floor($datediff / (60 * 60 * 24));


if ($dependantId =='Self') {
      $dob=$AfyaUserAge;
      $gender=$pdetails->gender;
    $firstName = $pdetails->firstname;
    $secondName = $pdetails->secondName;
    $name =$firstName." ".$secondName;
}

else {
     $dob=$dependantAge;
     $gender=$pdetails->depgender;
     $firstName = $pdetails->dep1name;
     $secondName = $pdetails->dep2name;
     $name =$firstName." ".$secondName;
}
$interval = date_diff(date_create(), date_create($dob));
$age= $interval->format(" %Y Year, %M Months, %d Days Old");

}
?>




<!--tabs Menus-->
@include('includes.doc_inc.topnavbar_v2')


<!--tabs Menus-->
  @include('includes.doc_inc.headmenu')

<div class="row wrapper border-bottom page-heading">
  <div class="ibox float-e-margins">
    <div class="col-lg-12">
       <div class="tabs-container">
<?php if ($pdetails->persontreated=='Self') {
                    ?>
                          @include('doctor.triage')
                    <?php }else { ?>

                @include('doctor.triage59')

                    <?php } ?>

             </div>
          </div>





</div><!--tfloat-e-margins-->
</div><!--row wrapper-->
@endsection
@section('script-test')

    <script type="text/javascript">
           $(document).ready(function(){



            $.ajaxSetup({
 headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});


      $(".chief").select2({
          placeholder: "Select chief compliant...",
          minimumInputLength: 2,
          ajax: {
              url: '/tagprv/chief',
              dataType: 'json',
              data: function (params) {
                  return {
                      q: $.trim(params.term)
                  };
              },
              processResults: function (data) {
                  return {
                      results: data
                  };
              },
              cache: true
          }
      });
       });
    </script>

@endsection
