@extends('layouts.doctor_layout')
@section('title', 'Patient History')
@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){
$Did = $Docdata->id;
$Name = $Docdata->name;

}
if($user->persontreated == 'Self'){
  $name = $user->firstname ." ". $user->secondName;
  $gender = $user->gender;
  $dob = $user->dob;
  $occupation = $user->occupation;
}else{

$name = $user->name1 ." ". $user->name2;
$gender = $user->depgender;
$dob = $user->depdob;
  $occupation = '';
}
$interval = date_diff(date_create(), date_create($dob));
$age= $interval->format(" %Y Years, %M Months Old");
$date1=$ptdetails->created_at;

$datetime = explode(" ",$date1);
$date2 = $datetime[0];

$height1 =$ptdetails->current_height;
if($height1){
$height =$height1/100;
$BMI =$ptdetails->current_weight/($height*$height);
}else{
  $BMI ='';
}


?>
<div class="row wrapper white-bg page-heading">
              <div class="col-lg-8">
                  <h2>PATIENT</h2>
                  <ol class="breadcrumb">
                      <li>
                          <a href="#">  <strong>Name:</strong> {{$name}}</a>
                      </li>
                      <li>
                          <strong>Gender:</strong> {{$gender}}<br>

                      </li>
                      <li class="active">
                          <strong>Age:</strong> {{$age}}
                      </li>
                  </ol>
              </div>
              <div class="col-lg-4">
                  <div class="title-action">

                      <a href="{{route('patienthistory',$user->appid)}}"  class="btn btn-primary"><i class="fa fa-angle-double-left"></i> Back </a>
                      <input name="b_print" type="button" class="btn btn-primary"   onClick="printdiv('div_print');" value=" Print ">

                  </div>
              </div>
          </div>
<div class="wrapper wrapper-content">
  <div class="row white-bg"  id="div_print">
  <div class="col-md-10 col-md-offset-1">
    <div class="ibox float-e-margins">

                      <div class="ibox-content">
                        <h3>PATIENT RECORDS</h3>
                        <h3>DATE : {{$date2}}</h3>
                          <table class="table table-bordered text-center">

                              <tbody>
                              <tr>
                                  <td colspan="2"><strong>PATIENT NAME</strong></td>  <td colspan="2">{{$name}}</td>
                              </tr>
                              <tr>
                                  <td colspan="2"><strong>DOB</strong></td>  <td colspan="2">{{$dob}}</td>
                              </tr>
                              <tr>
                                  <td colspan="2"><strong>OCCUPATION</strong></td> <td colspan="2">{{$occupation}}</td>
                              </tr>
                              <tr>
                                  <td><strong>HEIGHT </strong><small>cm</small></td> <td>{{$ptdetails->current_height}}</td>
                                  <td><strong>HR </strong> <small>b/min</small></td><td>{{$ptdetails->hr}}</td>
                              </tr>
                              <tr>
                                  <td><strong>WEIGHT </strong> <small>kg</small></td> <td>{{$ptdetails->current_weight}}</td>
                                  <td><strong>BP </strong> <small>mmHg</small></td><td>{{$ptdetails->systolic_bp}} / {{$ptdetails->diastolic_bp}} </td>
                              </tr>

                              <tr>
                                  <td><strong>BMI </strong> <small>kg/m2</small></td> <td>@if($BMI){{$BMI}}@endif</td>
                                  <td><strong>RR </strong> <small>breaths/min</small></td><td>{{$ptdetails->rr}}</td>
                              </tr>
                              <tr>
                                  <td><strong>RBS </strong> <small>mmol/l</small></td> <td>{{$ptdetails->rbs}}</td>
                                  <td><strong>TEMP </strong> <small>°C</small></td><td>{{$ptdetails->temperature}}</td>
                              </tr>

                              </tbody>
                          </table>

                      </div>

                      <div class="ibox-content">
                        <h3>PATIENT History</h3>
                          <p> @if($summary){{$summary->notes}}  @endif</p>
                          <br>
                          <h5>Current Medication</h5>
                            <p> @if($cmeds){{$cmeds->drugs}}  @endif</p>
                        </div>

                        <div class="ibox-content">
                          <h3>EXAMINATION FINDINGS</h3>
                          <table class="table table-bordered text-center">
@if($ge)
                              <tbody>

                              <tr>
                                  <td><strong>GNERAL EXAMINATION </strong></td> <td>{{$ge->g_examination}}</td>
                              </tr><tr>
                                  <td><strong>CVS </strong> </td><td>{{$ge->cvs}}</td>
                            </tr><tr>
                                  <td><strong>RS </strong> </td> <td>{{$ge->rs}}</td>
                            </tr><tr>
                                  <td><strong>PA </strong> </td><td>{{$ge->pa}} </td>
                              </tr><tr>
                                  <td><strong>CNS </strong></td> <td>{{$ge->cns}}</td>
                              </tr><tr>
                                  <td><strong>MSS </strong> </td><td>{{$ge->mss}}</td>
                              </tr><tr>
                                <td><strong>PERIPHERIES </strong> </td> <td>{{$ge->peripheries}}</td>
                              </tr>

                              </tbody>
                          </table>
                          @endif
                          </div>

                          <div class="ibox-content">
                            <h3>RESULTS SO FAR:</h3>
                           <p> @if($rsofar) {{$rsofar->notes}} @endif</p>
                            </div>


                            <ul class="list-group clear-list m-t">
  <h3>Diagnosis:</h3>
                         @foreach($diagnosis as $tst1)
                          <li class="list-group-item fist-item">
                        <span class="pull-right"></span>{{$tst1->name}}
                          </li>
                          @endforeach
                        </ul>
                            <div class="ibox-content">
                              <h3>IMP:</h3>
                                <p> @if($impression) {{$impression->notes}} @endif</p>
                              </div>
                              <div class="ibox-content">
                                <h3>PLAN : </h3>
                                <h5>Tests : </h5>

                                <ul class="list-group clear-list m-t">

                             @foreach($tsts as $tst)
                              <li class="list-group-item fist-item">
                            <span class="pull-right"></span>{{$tst->tname}}
                              </li>
                              @endforeach


                              @foreach($rady as $radst)
                              <?php
                              if ($radst->test_cat_id== '9') {
                                $ct=DB::table('ct_scan')->where('id', '=',$radst->test) ->first();
                                $test = $ct->name;
                                $type ='donectdoc';


                              } elseif ($radst->test_cat_id== 10) {
                                $xray=DB::table('xray')->where('id', '=',$radst->test) ->first();
                                $test = $xray->name;
                                $type ='donexraydoc';
                              } elseif ($radst->test_cat_id== 11) {
                                $mri=DB::table('mri_tests')->where('id', '=',$radst->test)->first();
                                $test = $mri->name;
                                $type ='donemridoc';
                              }elseif ($radst->test_cat_id== 12) {
                                $ultra=DB::table('ultrasound')->where('id', '=',$radst->test) ->first();
                                $test = $ultra->name;
                                $type ='doneultradoc';
                              }

                              ?>

            <li class="list-group-item fist-item">
            <span class="pull-right"></span>{{$test}}
            </li>
              @endforeach


<h5>Procedures : </h5>
@foreach($procedures as $prc)
<li class="list-group-item fist-item">
<span class="pull-right"></span>{{$prc->description}}
</li>
@endforeach

<h5>Prescriptions : </h5>
@foreach($prescriptions as $prsc)
<li class="list-group-item fist-item">
<span class="pull-right"></span>{{$prsc->drug_id}}
</li>
@endforeach


  </ul>

                                </div>






                  </div>




















              </div>






  </div>

</div>

    @endsection
    @section('script-test')


    @endsection
